\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage[brazilian]{babel}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{listings}
\usepackage{amsmath}
\usepackage[scaled=.95]{helvet}% helvetica as the origin of arial
\usepackage[helvet]{sfmath}    % for the mathematical enviroments
\renewcommand{\familydefault}{\sfdefault}

\lstset{showstringspaces=false}
%% ETH beamer theme
% Options: [default]
%   itemsblack/[itemsblue]: change color of bullets etc. to black/blue in
%   itemize style environments
%   [titlesblack]/titlesblue: change color of frame titles/subtitles to
%   black/blue
% \usetheme[itemsblack,titlesblack]{eth}
\usetheme{eth}

%% Theme uses ETH blue color by default. Can be changed to any color using this
%% command: 
\setbeamercolor{structure}{fg=ETHred}

%% Mandatory variables
\author{Filipe Saraiva}
\title{Complexidade e Classes de Problemas}
\subtitle{Metaheurísticas para Otimização Combinatória}
\institute{UFPA}
\date{\today}

\begin{document}

\begin{frame}
    \maketitle
\end{frame}

\begin{frame}{Conteúdo}
	\tableofcontents
\end{frame}

\section{Introdução}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Introdução}
 \begin{center}
  O que são\\
  Algoritmos?
 \end{center}
\end{frame}

\begin{frame}{Introdução}
Algumas possíveis definições:
 \begin{center}
 \begin{block}{(Cormen, et al, 2002)}
 \begin{quote}
  Qualquer procedimento computacional bem definido que toma algum valor ou
  conjunto de valores como entrada e produz algum valor ou conjunto de valores
  como saída.
 \end{quote}
 \end{block}
 \end{center}
\end{frame}

\begin{frame}{Introdução}
Podemos resolver um mesmo problema com diferentes métodos de resolução
(algoritmos).
\newline\newline
Portanto, como saber se um algoritmo é melhor que outro? Por que escolher uma
abordagem em detrimento de outra?
\newline
\begin{itemize}
 \item Quais métricas seriam utilizadas?
 \item Como realizar boas comparações entre algoritmos?
\end{itemize}
\end{frame}

\section{Medidas de Complexidade}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Medidas de Complexidade}
A melhor abordagem seria relacionar alguma métrica diretamente com o algoritmo, independente da tecnologia de implementação ou do hardware utilizado.
\newline\newline
Para tanto, utiliza-se as chamadas \textbf{medida de complexidade} de um
algoritmo, que indicará sua eficiência e permitirá compará-lo com seus pares.
\end{frame}

\begin{frame}{Contando Número de Operações}
Uma boa métrica diretamente relacionada com o algoritmo em si, que independe da
tecnologia utilizada, é a contagem do número de operações a serem realizadas.
\newline\newline
\begin{alertblock}{Atenção!}
É \textbf{contagem do número de operações a serem realizadas, não das
linhas de código!} -- esse aviso é útil para os casos em que se usam bibliotecas
de funções já implementadas.
\end{alertblock}
\end{frame}

\begin{frame}[fragile]{Contando Número de Operações}
Exemplo 1: Algoritmo de busca do maior elemento em uma lista (fazendo tamanho da
lista igual a \textit{n}):
\newline
\begin{lstlisting}[language=Python]
def buscarMaiorElemento(lista):
    maior = lista[0]        # (1)
    for i in lista:         # (n)
        if maior < i:       # (n)
            maior = i       # (0 a n)
    return maior            # (1)
\end{lstlisting}
\ \newline
O número total de operações irá variar entre:\\
$1 + n + n + 0 + 1 = 2n + 2$\\ e\\ $1 + n + n + n + 1 = 3n + 2$.
\end{frame}

\begin{frame}{Contando Número de Operações}
Como apresentado no exemplo, percebemos que o número de operações é proporcional
à entrada de dados para o algoritmo.
\newline\newline
O número de operações varia tanto pelo tamanho da entrada quanto pela disposição
de seus elementos -- por exemplo, uma lista organizada em ordem decrescente pode
implicar em diferente número de operações para ordená-la de forma crescente.
\end{frame}

\begin{frame}{Contando Número de Operações}
Algumas vantagens dessa métrica:
\newline
\begin{itemize}
 \item Intrínseca ao algoritmo;
 \item Independe de hardware, linguagem de programação, etc;
 \item Estima o ``tempo de processamento'' esperado a partir do número de
 operações;
 \item Permite a comparação entre algoritmos sem relacioná-los a características
 estranhas a eles.
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Contando Número de Operações}
Variação de tempo a partir do tamanho da entrada e complexidade para um
computador que processa 1 operação a cada microssegundo:
\newline
{%
\footnotesize
\newcommand{\mc}[3]{\multicolumn{#1}{#2}{#3}}
\begin{table}
\begin{center}
\begin{tabular}{c|cccccc}
\hline
\ & \mc{6}{c}{Tamanho de \textit{n}}\\\hline
Função\\Custo & 10 & 20 & 30 & 40 & 50 & 60\\\hline
$n$ & 0,00001s & 0,00002s & 0,00003s & 0,00004s & 0,00005s & 0,00006s\\
$n^2$ & 0,0001s & 0,0004s & 0,0009s & 0,0016s & 0,0035s & 0,0036s\\
$n^3$ & 0,001s & 0,008s & 0,027s & 0,64s & 0,125s & 0,316s\\
$n^5$ & 0,1s & 3,2s & 24,3s & 1,7min & 5,2min & 13min\\
$2^n$ & 0,001s & 1s & 17,9min & 12,7dias & 35,7anos & 366séc.\\
$3^n$ & 0,059s & 58min & 6,5anos & 3855séc. & $10^8$séc. & $10^{13}$séc.\\\hline
\end{tabular}
\end{center}
\end{table}
}%
\end{frame}

\begin{frame}{Notação Assintótica}
Como o número de operações realizadas depende da entrada, qual quantidade de
operações devemos assumir como o valor da métrica para um dado algoritmo?
\newline\newline
É possível utilizar tanto as de melhor caso (menor número de operações) quanto
as do caso médio ou pior caso;
\newline
\begin{exampleblock}{Qual utilizar então?}
Na área é mais comum utilizarmos a \textbf{medição no pior caso}.
\end{exampleblock}
\end{frame}

\begin{frame}{Notação Assintótica}
Chama-se \textbf{Complexidade Assintótica} a complexidade do algoritmo no pior
caso. Por que ela é a mais utilizada?
\newline
\begin{itemize}
 \item Normalmente, se um algoritmo se comporta bem para o pior caso, ele se
 comportará bem também nos melhores;
 \item Nos faz projetar os algoritmos com maior zelo, levando-se em conta que o
 pior caso pode ocorrer.
\end{itemize}
\end{frame}

\begin{frame}{Notação Assintótica}
A notação da complexidade assintótica chama-se notação assintótica, representada
por \textbf{O(função)}.
\newline\newline
Para calcularmos a função de complexidade, procedemos com o cálculo do número de operações para o pior caso e utilizamos apenas a variável e índice de maior grau
da função.
\end{frame}

\begin{frame}[fragile]{Contando Número de Operações}
Exemplo: Imprimir os elementos de uma matriz quadrada (fazendo 
\textit{len(matriz) = n} -- número de linhas da matriz é igual a \textit{n}):
\newline
\begin{lstlisting}[language=Python,mathescape=true]
def imprimirMatriz(matriz):
    for i in matriz:    # (n)
        for j in i:     # ($n^2$)
            print(j)    # ($n^2$)
\end{lstlisting}
\ \newline
O número total de operações será $n + n^2 + n^2 = 2n^2 + n$\\
Portanto, a complexidade assintótica do algoritmo é \textbf{O(n$^2$)}.
\end{frame}

\section{Exemplo: Ordenação}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Ordenação}
 Um bom caso para verificarmos algoritmos diferentes com complexidades
 diferentes aplicados a um mesmo problema: Problema de Ordenação.
\end{frame}

\begin{frame}{Ordenação}
 Dado uma lista desordenada de números inteiros, o problema consiste em
 ordená-los de forma crescente.
 \newline\newline
 Existem diversos algoritmos de ordenação disponíveis para esse problema, cada
 um com suas características e complexidade.
\end{frame}

\begin{frame}{Ordenação}
 Para o exemplo podemos citar os seguintes, bem como suas complexidades:
 \begin{itemize}
  \item BubbleSort: O(n$^2$)
  \item MergeSort: O(n log$_2$ n)
 \end{itemize}
\end{frame}

\begin{frame}{Ordenação}
\begin{center}
 \includegraphics[scale=0.3]{n-nlogn.png}\\
\end{center}
No gráfico temos o BubbleSort em azul, apresentando o comportamento da sua
função de complexidade assintótica (n$^2$) e o MergeSort em verde (n log$_2$ n).
\end{frame}

\begin{frame}{Ordenação}
 Além desses algoritmos, poderíamos pensar em outro de pior complexidade.\\
 Imagine que os algoritmos de ordenação não sejam conhecidos e para resolver o
 problema seria necessário listar todas as possibilidades de ordenação da lista.
 \newline\newline
 O número de possibilidades seria $n!$, o que acarretaria na complexidade
 assintótica \textbf{O(n!)}.
\end{frame}

\begin{frame}{Ordenação}
\begin{center}
 \includegraphics[scale=0.3]{n-nlogn-nfat.png}\\
\end{center}
A função que lista todas as soluções possíveis, n!, está em preto.
\end{frame}

\begin{frame}{Ordenação}
 A função de complexidade \textbf{O(n!)} se comporta de forma muito pior que as
 demais, demandando muito mais execuções de funções e tempo do que os algoritmos
 \textbf{O(n$^2$)} e \textbf{O(n log$_2$ n)} para uma mesma entrada de tamanho
 a partir de 3.
\end{frame}

\section{Classes de Problemas}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Classes de Problemas}
 Nessa apresentação foram verificados conceitos relacionados com a complexidade
 de algoritmos. Agora vejamos conceitos mais relacionados com os problemas em
 si.
 \newline\newline
 \textbf{Complexidade de um problema} computacional é o consumo de tempo do
 melhor algoritmo possível para aquele problema no pior caso.
\end{frame}

\begin{frame}{Classes de Problemas}
 Dizer que um \textbf{algoritmo resolve um problema} significa que se o
 algoritmo receber qualquer instância daquele problema ele retornará com a
 solução (ou informará que não há solução).
 \newline\newline
 Quando um algoritmo é polinomial significa que o consumo de tempo no pior caso
 é limitado por um polinômio do tamanho das instâncias do problema.
\end{frame}

\begin{frame}{Classes de Problemas}
 Exemplos de problemas com algoritmos polinomiais:
 \begin{itemize}
  \item Problema de ordenação (\textbf{O(n log$_2$ n)});
  \item Cálculo do fatorial (\textbf{O(n)});
  \item Caminho mínimo entre dois vértices de um grafo 
  \textbf{(O(n log$_2$ n))};
  \item Cálculo da arvore geradora mínima (\textbf{O(n log$_2$ n)});
  \item Percorrer matriz bidimensional (\textbf{O(n$^2$)});
 \end{itemize}
 Cabe notar que o polinômio pode ter um índice muito grande que ainda assim o
 algoritmo será polinomial.
\end{frame}

\begin{frame}{Classes de Problemas}
 Se para um problema existe um algoritmo polinomial que o resolva, então esse
 \textbf{problema é polinomial}. Problemas desse tipo são considerados fáceis ou
 \textbf{tratáveis}.
 \newline\newline
 A \textbf{Classe P} compreende todos os problemas polinomiais.
\end{frame}

\begin{frame}{Classes de Problemas}
 Quando um certo problema, polinomial ou não, pode ter a solução de algumas de
 suas instâncias verificada em tempo polinomial, dizemos que o problema pertence
 a \textbf{Classe NP} (\textit{nondeterministic polynomial}).
 \newline\newline
 É fácil verificar que \textbf{P} $\subset$ \textbf{NP}, mas até o momento foi
 impossível constatar se \textbf{P} = \textbf{NP}, que é um dos grandes
 problemas da matemática em aberto.
\end{frame}

\begin{frame}{Classes de Problemas}
 Ocorre que existem diversos problemas para os quais não se conhece um algoritmo
 polinomial para resolução, ao passo que dado uma solução é possível verificar
 se ela resolve ou não o problema.
 \newline\newline
 Os problemas dito \textbf{NP Difícil} são problemas tais quais são tão
 difíceis quanto o mais difícil problema conhecido dessa classe.
\end{frame}

\begin{frame}{Classes de Problemas}
 Quando um problema é \textbf{NP} e \textbf{NP Difícil}, então ele pertence à
 \textbf{Classe NP Completo}. Problemas desse tipo são considerados
 \textbf{intratáveis}.
 \newline\newline
 Boa parte dos Problemas de Otimização Combinatória pertencem à classe NP
 Completo.
\end{frame}

\begin{frame}{Classes de Problemas}
 \begin{center}
 \includegraphics[scale=0.6]{grafico-complexidade.eps}\\
\end{center}
Gráfico das Classes de Problemas apresentadas
\end{frame}

\section{Exemplo: Problema do Caixeiro Viajante}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Problema do Caixeiro Viajante}
 Um dos Problemas de Otimização Combinatória mais conhecidos e desenvolvidos na
 literatura é o Problema do Caixeiro Viajante, que é NP Completo.
 \newline\newline
 A descrição do problema é a seguinte: ``Dado um conjunto de cidades e um
 conjunto de estradas que ligam essas cidades, sair de alguma delas e visitar
 todas as demais, apenas uma vez, e voltar à cidade inicial, no menor caminho
 possível''.
\end{frame}

\begin{frame}{Problema do Caixeiro Viajante}
 \begin{center}
 \includegraphics[scale=0.4]{caixeiro-viajante.png}\\
\end{center}
Instância do Problema do Caixeiro Viajante
\end{frame}

\begin{frame}{Problema do Caixeiro Viajante}
 Matematicamente, é possível modelar as cidades e as estradas como um grafo,
 fazendo com que o problema passe a ser encontrar o ciclo hamiltoniano mínimo.
 \newline\newline
 Não há um algoritmo polinomial conhecido para resolver esse problema. Portanto,
 para obtermos a solução ótima, é necessário listar todas as soluções possíveis.
 \newline\newline
 O número de soluções possíveis é $((n - 1)!/2) + 1$, onde $n$ é o número de
 cidades.
\end{frame}

\begin{frame}{Problema do Caixeiro Viajante}
Se seu computador conseguir computar 1 milhão de ciclos hamiltonianos por
segundo:
{%
\footnotesize
\begin{table}
\begin{center}
\begin{tabular}{c|cccccccc}
\hline
\ & {Tamanho de \textit{n}}\\\hline
Função\\Custo & 9 & 10 & 11 & 12 & 13 & 14 & 15 & 20\\\hline
$((n - 1)!/2) + 1$ & Instantâneo & 0,33s & 4s & 40s & 9min & 2h & $\simeq$ 1 
dia & 1mi anos\\\hline
\end{tabular}
\end{center}
\end{table}
}%
\end{frame}

\section{Conclusões}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Conclusões}
\begin{itemize}
 \item Algoritmos são procedimentos ordenados que permitem a resolução de
 determinado problema;
 \item Para comparar algoritmos utilizamos notação assintótica;
 \item Há classes de problemas computáveis a partir dos melhores algoritmos
 existentes para resolver os problemas;
 \item No geral, problemas de otimização combinatória são do tipo NP Completo,
 não existindo até o momento algoritmos polinomiais de resolução;
 \item Para estes casos, utilizam-se abordagens heurísticas para encontrar uma
 solução boa e aceitável para estes problemas.
\end{itemize}
\end{frame}

\begin{frame}
    \maketitle
\end{frame}

\end{document}
