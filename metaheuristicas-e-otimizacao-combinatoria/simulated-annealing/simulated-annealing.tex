\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage[brazilian]{babel}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{amsmath}
\usepackage{algorithmic}
\usepackage{textcomp}
\usepackage[scaled=.95]{helvet}% helvetica as the origin of arial
\usepackage[helvet]{sfmath}    % for the mathematical enviroments
\renewcommand{\familydefault}{\sfdefault}

\input{algorithmic-portuguese}

%% ETH beamer theme
% Options: [default]
%   itemsblack/[itemsblue]: change color of bullets etc. to black/blue in
%   itemize style environments
%   [titlesblack]/titlesblue: change color of frame titles/subtitles to
%   black/blue
% \usetheme[itemsblack,titlesblack]{eth}
\usetheme{eth}

%% Theme uses ETH blue color by default. Can be changed to any color using this
%% command: 
\setbeamercolor{structure}{fg=ETHred}

%% Mandatory variables
\author{Filipe Saraiva}
\title{Arrefecimento Simulado}
\subtitle{Metaheurísticas para Otimização Combinatória}
\institute{UFPA}
\date{\today}

\begin{document}

\begin{frame}
    \maketitle
\end{frame}

\begin{frame}{Conteúdo}
	\tableofcontents
\end{frame}

\section{Introdução}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Introdução}
  Nessa apresentação veremos os conceitos fundamentais da metaheurística
  \textbf{Arrefecimento Simulado}, mais conhecida como \textit{Simulated
  Annealing} (SA).
  \newline\newline
  Apesar de utilizarmos na aula o nome em português, é mais comum encontrarmos
  mesmo na literatura em nossa língua o nome original em inglês do método.
\end{frame}

\section{Arrefecimento Simulado}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Arrefecimento Simulado}
  O Arrefecimento Simulado é baseado no processo de aquecimento/resfriamento de
  metais e vidros empregados por artífices para fazer utensílios, peças e obras
  de arte.
  \newline\newline
  Nesse processo, a matéria-prima é aquecida até ficar completamente maleável,
  quando então o artífice molda-a para que ela adquira a forma por ele
  pretendida.
\end{frame}

\begin{frame}{Arrefecimento Simulado}
  \begin{center}
   \includegraphics[scale=0.35]{glass-annealing.jpg}\\
   Exemplo de fabricação de garrafas por arrefecimento
  \end{center}
\end{frame}

\begin{frame}{Arrefecimento Simulado}
  Quando a temperatura está muito alta, a matéria-prima está completamente
  maleável, podendo assumir qualquer forma. A medida que a temperatura vai
  diminuindo, a matéria vai reduzindo a maleabilidade, podendo assumir apenas
  pequenas mudanças em sua forma.
  \newline\newline
  Há um limite de decaimento da temperatura quando a matéria atinge um estado de
  estabilidade, quando não é possível mais nenhuma mudança.
\end{frame}

\begin{frame}{Arrefecimento Simulado}
  Essa é a analogia utilizada pelo SA. No início, a partir de uma solução,
  podemos modificá-la livremente, inclusive assumindo valores que pioram a
  função objetivo.
  \newline\newline
  Há uma variável de controle que representa a temperatura e o seu resfriamento.
  A medida que ela diminui, as mudanças para soluções piores vão sendo mais
  difíceis de serem realizadas, sendo mais fáceis apenas as mudanças para
  soluções melhores.
  \newline\newline
  O método termina quando se atinge um ponto onde nenhuma modificação é mais
  possível, quando dizemos que a solução ``estabilizou'' -- ou convergiu.
\end{frame}

\begin{frame}{Arrefecimento Simulado}
  Portanto, o SA tem as seguintes características:
  \begin{itemize}
   \item Aplicado sobre uma solução única;
   \item Definição de uma função para gerar a vizinhança;
   \item Iterações contadas a partir de uma ``função de resfriamento'';
   \item Quanto maior for a temperatura, maiores as chances da solução mudar
   para uma vizinha com desempenho pior na função objetivo;
   \item O método pára quando a temperatura tiver atingido um nível delimitado
   para ser o critério de parada.
  \end{itemize}
\end{frame}

\section{Etapas do Arrefecimento Simulado}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Etapas do Arrefecimento Simulado}
  Detalharemos a seguir as principais etapas do SA, a saber:
  \begin{itemize}
   \item Geração da solução inicial;
   \item Geração do vizinho;
   \item Função de movimento para o vizinho;
   \item Número de análises de vizinhos;
   \item Função de resfriamento;
   \item Critério de parada.
  \end{itemize}
\end{frame}

\begin{frame}{Geração da Solução Inicial}
  Como um método que trabalha a partir de uma solução inicial, o SA precisa
  gerar essa solução de alguma forma.
  \newline\newline
  Quando não se conhece a instância do problema a ser trabalhada, no geral essa
  solução é criada de forma aleatória. Com algum conhecimento do problema, é
  possível definir partes da solução e gerar outras de forma aleatória, mas isso
  nem sempre é possível.
\end{frame}

\begin{frame}{Geração do Vizinho}
  Para que o SA navegue no espaço de busca, é necessário uma função que gere
  vizinhos para a solução em análise.
  \newline\newline
  Como em outras funções da metaheurística, essa definição dependerá bastante do
  problema ao qual se está abordando.
  \newline\newline
  A função de geração de vizinhos é de grande importância pois é ela quem irá
  definir a qualidade da busca a ser realizada.
  \newline\newline
  A função irá gerar 1 vizinho apenas, que será avaliado posteriormente. Essa
  função deve ter algum componente aleatório para garantir que diferentes
  vizinhos possam ser gerados em uma dada iteração.
\end{frame}

\begin{frame}{Movimento para o Vizinho}
  Com o vizinho gerado, o SA pode calcular a função objetivo alcançada pela
  solução. Em seguida, faz-se o cálculo da diferença entre as funções objetivos
  da solução atual ($s_o$) e do vizinho (solução em análise -- $s_i$):
  \begin{center}
   $\Delta s = s_i - s_o$
  \end{center}
  O que, para um problema de minimização, nos levará às seguintes conclusões (o
  inverso para problemas de maximização):
  \begin{itemize}
   \item $\Delta s < 0$ - a nova solução é melhor que a autal;
   \item $\Delta s = 0$ - indiferente;
   \item $\Delta s > 0$ - a nova solução é pior que a atual;
  \end{itemize}
\end{frame}

\begin{frame}{Movimento para o Vizinho}
  A partir do valor de $\Delta s$ é realizada a tomada de decisão sobre o
  movimento para o vizinho a partir da seguinte verificação:
  \begin{itemize}
   \item Se $\Delta s < 0$ - muda para o vizinho (para problemas de
   minimização);
   \item $\Delta s \geq 0$ - há uma probabilidade para se mudar ou não para esse
   vizinho;
  \end{itemize}
\end{frame}

\begin{frame}{Movimento para o Vizinho}
  Essa probabilidade é dada por uma função que depende da temperatura do
  sistema. No geral, a função é definida por uma distribuição de Boltzmann dada
  por:
  \begin{center}
   $e^{- \Delta s / t} > r$
  \end{center}
  Onde $e$ é o número de Euler (2,7182...), $t$ é a temperatura atual do
  sistema, e $r$ é um número aleatório no intervalo [0, 1].
  \newline\newline
  Caso o lado esquerdo da inequação seja maior que $r$, a solução passa a ser o
  vizinho em análise.
  Caso contrário, a solução não muda e realiza-se a geração de um novo vizinho.
\end{frame}

\begin{frame}{Movimento para o Vizinho}
  Quanto maior for $t$, maior será a probabilidade do SA aceitar o vizinho de
  pior desempenho na função objetivo como nova solução.
  \newline\newline
  Com o decorrer da execução do método e a diminuição de $t$, fica mais difícil
  movimentar para esses vizinhos pois o valor da equação diminuirá, forçando a
  convergência do método para um ótimo local.
  \newline\newline
  No limite, temos o seguinte comportamento para valores de $t$:
  \begin{itemize}
   \item $t$ \textrightarrow $\infty$ -- busca aleatória;
   \item $t$ \textrightarrow 0 -- busca local.
  \end{itemize}
\end{frame}

\begin{frame}{Número de Análises de Vizinhos}
  A análise de vizinhança é realizada algumas vezes até se atingir um critério
  de parada e, então, seguir para a função de resfriamento.
  \newline\newline
  Normalmente utilizam-se critérios estatícos, como a definição de um número
  máximo de iterações, para esta parada. Outra forma de definição de critério
  é utilizar um número máximo de iterações sem alterações na solução em análise.
\end{frame}

\begin{frame}{Função de Resfriamento}
  Mimetizando o comportamento do arrefecimento, no SA existe uma função que mede
  o decaimento da temperatura, que terá como consequência a redução da
  probabilidade de soluções piores que a atual possam ser assumidas pelo método.
  \newline\newline
  A função de atualização da temperatura é executada quando o critério de parada
  da análise de vizinhança é atingido. Após atualizar a temperatura, o SA
  executa mais análises de vizinhança.
\end{frame}

\begin{frame}{Função de Resfriamento}
  É importante notar que uma função de resfriamento que reduz de forma vagarosa
  a temperatura permite uma maior exploração do espaço de busca, permitindo ao
  SA assumir soluções que o levam a explorar novos subespaços de busca.
  Entretanto, isso aumenta o tempo computacional do método.
  \newline\newline
  Por outro lado, funções que resfriam muito rápido aumentam a intensificação do
  SA, tornando-o muito rápido, porém impedindo que o mesmo faça uma busca mais
  ampla no espaço de soluções.
\end{frame}

\begin{frame}{Função de Resfriamento}
  Há diversas formas de implementar o resfriamento, as mais comuns sendo (com
  $t_i$ sendo a temperatura atual e $t_{i + 1}$ a próxima temperatura):
  \begin{itemize}
   \item Linear ($t_{i + 1} = t_i - \beta$) -- uma redução constante no valor
   $\beta$ a cada iteração;
   \item Geométrica ($t_{i + 1} = \alpha t_i$), com $\alpha \in$ ]0, 1[ --
   redução via multiplicação;
   \item Logaritmica ($t_{i + 1} = t_i / log(i)$);
   \item Adaptativa -- diferentes formas de resfriamento a partir da iteração;
   \item ...
  \end{itemize}
\end{frame}

\begin{frame}{Função de Resfriamento}
  O tema da função de resfriamento nos chama atenção para um parâmetro
  importante do método: a temperatura inicial.
  \newline\newline
  Se ela for muito alta, o processo de busca poderá ser muito aleatório nas
  primeiras iterações. Caso seja baixa, a busca será pouco diversificada.
  \newline\newline
  Estratégias comuns de definição são escolher uma temperatura aleatória,
  definir uma temperatura cuja aplicação da função de resfriamento dê um valor
  de 80\% para a solução inicial, entre outras.
\end{frame}

\begin{frame}{Critério de Parada}
  O critério de parada mais utilizado para o SA é quando a temperatura atinge
  um certo valor limite. Esse valor normalmente é calculado como 1\% da
  temperatura inicial, mas outros podem ser utilizados.
\end{frame}

\section{Pseudocódigo}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Pseudocódigo}
  Os principais parâmetros do SA que devem ser decididos durante o projeto do
  método são:
  \begin{itemize}
   \item Temperatura inicial;
   \item Geração do vizinho;
   \item Número de vizinhos a serem avaliados;
   \item Função de resfriamento;
   \item Critério de parada.
  \end{itemize}
  A seguir tem-se o pseudocódigo para o SA nos casos onde ele é aplicado a um
  problema de minimização (para problemas de maximização, as verificações de
  $\Delta$S deverão ser invertidas).
\end{frame}

\begin{frame}[fragile]{Pseudocódigo}
  \begin{algorithmic}
   \STATE Gera solução inicial
   \REPEAT
    \REPEAT
      \STATE Gera vizinho
      \STATE Calcula $\Delta$S
      \IF{$\Delta$S < 0}
        \STATE Assume vizinho como nova solução
      \ELSE
        \STATE Calcula probabilidade de assumir o vizinho
      \ENDIF
    \UNTIL{Número de vizinhos a serem avaliados}
    \STATE Calcula função de resfriamento
   \UNTIL{Atingir critério de parada}
  \end{algorithmic}
\end{frame}

\section{Conclusões}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Conclusões}
 \begin{itemize}
  \item O método SA realiza a otimização com base no processo de resfriamento de
  materiais como vidro e ferro;
  \item O SA é um método de solução única;
  \item O SA realiza uma busca na vizinhança e verifica se uma nova solução pode
  ser assumida em, quando pior que a atual, a partir de uma probabilidade
  mediada pela ``função de resfriamento'' do método.
 \end{itemize}
\end{frame}

\begin{frame}
    \maketitle
\end{frame}

\end{document}
