\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage[brazilian]{babel}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{amsmath}
\usepackage[scaled=.95]{helvet}% helvetica as the origin of arial
\usepackage[helvet]{sfmath}    % for the mathematical enviroments
\renewcommand{\familydefault}{\sfdefault}

%% ETH beamer theme
% Options: [default]
%   itemsblack/[itemsblue]: change color of bullets etc. to black/blue in
%   itemize style environments
%   [titlesblack]/titlesblue: change color of frame titles/subtitles to
%   black/blue
% \usetheme[itemsblack,titlesblack]{eth}
\usetheme{eth}

%% Theme uses ETH blue color by default. Can be changed to any color using this
%% command: 
\setbeamercolor{structure}{fg=ETHred}

%% Mandatory variables
\author{Filipe Saraiva}
\title{Introdução a Metaheurísticas}
\subtitle{Metaheurísticas para Otimização Combinatória}
\institute{UFPA}
\date{\today}

\begin{document}

\begin{frame}
    \maketitle
\end{frame}

\begin{frame}{Conteúdo}
	\tableofcontents
\end{frame}

\section{Introdução}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Introdução}
  Nessa apresentação veremos alguns conceitos preliminares sobre
  metaheurísticas, em especial a diferença entre elas e as heurísticas.
\end{frame}

\section{Metaheurísticas -- Conceitos}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Metaheurísticas}
  Dados problemas intratáveis porém cujas resoluções são de grande importância,
  a abordagem heurística surge como uma técnica onde abre-se mão da busca pela
  solução ótima em troca de se encontrar uma solução boa em tempo computacional
  hábil.
  \newline\newline
  Entretanto, a abordagem heurística costuma levar a busca para ótimos locais.
\end{frame}

\begin{frame}{Metaheurísticas}
  Para tentar realizar uma melhor exploração do espaço de busca, foram
  desenvolvidas as \textbf{metaheurísticas}.
\end{frame}

\begin{frame}{Metaheurísticas}
  Em termos grosseiros, imagine que metaheurísticas são heurísticas guiadas por
  outras heurísticas, que permitem a avaliação de soluções piores que alguma
  atual, na expectativa de que, por ventura, ótimos locais possam ser superados
  em busca de melhores ótimos locais (não necessariamente, ótimo global).
\end{frame}

\begin{frame}{Metaheurísticas}
  \begin{block}{Conceito}
   \textbf{Metaheurísticas} são métodos de resolução heurísticos, onde busca-se
   uma boa solução para o problema em tempo computacional hábil. Nesses métodos,
   soluções piores que alguma atual em avaliação podem ser assumidas na
   expectativa de, mais à frente, encontrar melhores soluções para o problema.
  \end{block}
\end{frame}

\begin{frame}{Metaheurísticas}
  Cabe destacar que as metaheurísticas, assim como as heurísticas, podem
  encontrar até a solução ótima, mas não é esse o objetivo do método (e, no
  geral, não temos como saber se a solução encontrada é a ótima).
\end{frame}

\begin{frame}{Metaheurísticas}
  Quanto a maneira como as metaheurísticas desenvolvem soluções, elas podem ser
  classificadas em dois tipos:
  \begin{itemize}
   \item Solução única: trabalha com apenas uma solução;
   \item Populacional: avalia um conjunto de soluções simultaneamente.
  \end{itemize}
\end{frame}

\begin{frame}{Metaheurísticas}
  Uma Metaheurística é composta necessariamente pelas seguintes funções:
  \begin{itemize}
   \item Vizinhança: o método tem uma maneira de gerar soluções vizinhas no
   espaço de busca e avaliá-las;
   \item Diversificação: a metaheurística tem uma função que permite atingir
   estados não vizinhos, alterando a busca e permitindo a fuga do ótimo local;
   \item Avaliação: critério que permite avaliar, entre um conjunto de decisões
   possíveis, qual a que será realizada.
  \end{itemize}
\end{frame}

\begin{frame}{Metaheurísticas}
  Em uma Metaheurística, o que guia a avaliação será uma combinação tanto da
  escolha pela melhor solução atual mas com a possibilidade da escolha recair
  sobre uma pior. Esta probabilidade é dada pela função de diversificação do
  método.
  \newline\newline
  Isso faz com que os métodos metaheurísticos sejam bons em explorar o espaço de
  busca de soluções do problema, realizando tanto uma busca global (avaliando
  diferentes subpartes desse espaço) como intensificando em algum subespaço
  quando necessário.
\end{frame}

\begin{frame}{Metaheurísticas}
  Metaheurísticas:
  \begin{center}
   Intensificação\\
   vs\\
   Exploração
  \end{center}
\end{frame}

\begin{frame}{Metaheurísticas}
  Relembrando o método Subida/Descida de Encosta, o comportamento de uma
  metaheurística no espaço de busca de soluções pode ser imaginado da seguinte
  forma:
\end{frame}

\begin{frame}{Metaheurísticas}
  \begin{center}
    \includegraphics[scale=0.55]{solucao-inicial.eps}\\
  \end{center}
  Solução inicial tanto para uma heurística quanto uma metaheurística
  (minimização).
\end{frame}

\begin{frame}{Metaheurísticas}
  \begin{center}
    \includegraphics[scale=0.55]{solucao-inicial-direcao.eps}\\
  \end{center}
  Direção da otimização. Tanto a heurística quanto a metaheurística irão
  segui-la.
\end{frame}

\begin{frame}{Metaheurísticas}
  \begin{center}
    \includegraphics[scale=0.55]{solucao-alcancada.eps}\\
  \end{center}
  Nova solução alcançada tanto pela heurística quanto pela metaheurística.
\end{frame}

\begin{frame}{Metaheurísticas}
  \begin{center}
    \includegraphics[scale=0.55]{direcao-piora.eps}\\
  \end{center}
  Se continuar na direção, as soluções adotadas serão piores que a em 
  avaliação. Uma metaheurística poderá assumí-las.
\end{frame}

\begin{frame}{Metaheurísticas}
  \begin{center}
    \includegraphics[scale=0.55]{solucao-final.eps}\\
  \end{center}
  No exemplo, a metaheurística atinge um novo ótimo local (que também é um
  ótimo global) melhor que a solução encontrada pela heurística.
\end{frame}

\section{Metaheurísticas e Computação Natural}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Metaheurísticas e Computação Natural}
  Uma situação curiosa ocorre com os pesquisadores da área de metaheurísticas. É
  muito comum que métodos se baseiem em dinâmicas encontradas na natureza para
  realizar a otimização nos problemas.
  \newline\newline
  Uma justificativa sempre citada é que a natureza resolve problemas de
  otimização com essas dinâmicas, portanto mimetizá-las seria uma forma de
  abordar esses problemas com métodos testados e desenvolvidos ao longo de
  milênios.
\end{frame}

\begin{frame}{Metaheurísticas e Computação Natural}
  Por conta disso é muito comum que a área seja as vezes referenciada como
  \textbf{Computação Natural}, \textbf{Computação Evolutiva}, \textbf{Computação
  Bioinspirada}, \textit{\textbf{Soft Computing}}, entre outros.
\end{frame}

\begin{frame}{Metaheurísticas e Computação Natural}
  Algumas metaheurísticas e suas analogias:
  \begin{itemize}
   \item Arrefecimento simulado -- aquecimento/resfriamento de metais e vidro;
   \item Busca tabu -- processo de memorização;
   \item Algoritmos genéticos -- dinâmica evolutiva das espécies;
   \item Colônia de formigas -- processo de busca de alimento por formigas;
   \item Enxame de partículas -- comportamento de bando em cardumes e enxames;
   \item Busca harmônica --  a maneira como os jazzistas improvisam;
   \item ...
  \end{itemize}
\end{frame}

\begin{frame}{Metaheurísticas e Computação Natural}
  Um efeito negativo dessa característica, ao menos para os interessados nas
  aplicações e resoluções de problemas, é que criou-se uma situação onde
  tenta-se transformar tudo o que é observado na natureza em metaheurística
  (exagero meu mas enfim).
  \newline\newline
  Muitas vezes, em termos de otimização, algumas metaheurísticas nada diferem
  ou pouco acrescentam às já existentes.
\end{frame}

\section{Conclusões}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Conclusões}
 \begin{itemize}
  \item Metaheurísticas são heurísticas onde há um método heurístico que guia a
  busca;
  \item Metaheurísticas tentam balancear exploração com intensificação,
  escapando de ótimos locais para tentar chegar a melhores soluções;
  \item É muito comum que dinâmicas encontradas na natureza sejam
  ``mimetizadas'' por métodos metaheurísticos na busca por solucionar problemas
  de otimização.
 \end{itemize}
\end{frame}

\begin{frame}
    \maketitle
\end{frame}

\end{document}
