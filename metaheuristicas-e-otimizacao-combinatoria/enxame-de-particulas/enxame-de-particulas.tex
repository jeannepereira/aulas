\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage[brazilian]{babel}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{amsmath}
\usepackage{algorithmic}
\usepackage{textcomp}
\usepackage[scaled=.95]{helvet}% helvetica as the origin of arial
\usepackage[helvet]{sfmath}    % for the mathematical enviroments
\renewcommand{\familydefault}{\sfdefault}

\input{algorithmic-portuguese}

%% ETH beamer theme
% Options: [default]
%   itemsblack/[itemsblue]: change color of bullets etc. to black/blue in
%   itemize style environments
%   [titlesblack]/titlesblue: change color of frame titles/subtitles to
%   black/blue
% \usetheme[itemsblack,titlesblack]{eth}
\usetheme{eth}

%% Theme uses ETH blue color by default. Can be changed to any color using this
%% command: 
\setbeamercolor{structure}{fg=ETHred}

%% Mandatory variables
\author{Filipe Saraiva}
\title{Enxame de Partículas}
\subtitle{Metaheurísticas para Otimização Combinatória}
\institute{UFPA}
\date{\today}

\begin{document}

\begin{frame}
    \maketitle
\end{frame}

\begin{frame}{Conteúdo}
	\tableofcontents
\end{frame}

\section{Introdução}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Introdução}
  Nessa apresentação veremos os conceitos sobre a metaheurística Enxame de
  Partículas (do inglês, \textit{Particle Swarm Optimization} -- PSO).
\end{frame}

\section{Enxame de Partículas}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Enxame de Partículas}
  Enxame de Partículas é um método metaheurístico populacional membro da família
  dos métodos de ``inteligência de enxame'', que são técnicas de inteligência
  artificial onde ações isoladas tomadas por agentes do método conduzem a
  emergência de um comportamento global produzido pelo conjunto de agentes.
\end{frame}

\begin{frame}{Enxame de Partículas}
  O paper seminal sobre o PSO foi publicado por Kennedy e Eberhart em 1995.
  Seguido a esse estudo, outros modelos e operadores para o PSO foram propostos
  por esses autores em publicações subsequentes.
\end{frame}

\begin{frame}{Enxame de Partículas}
  A principal inspiração do Enxame de Partículas vem do comportamento social
  encontrado em enxames de abelhas, revoada de pássaros, cardumes de peixes, e
  outros.
  \newline\newline
  Esses animais conseguem coordenar o deslocamento de um grande conjunto de
  indivíduos de forma ordeira, sem comando centralizado.
\end{frame}

\begin{frame}{Enxame de Partículas}
  Os indivíduos mais destacados na tarefa em execução (por exemplo, o peixe mais
  distante do cardume na tarefa de deslocamento), influencia os demais e é
  também influenciado por estes, otimizando a rota produzida.
\end{frame}

\begin{frame}{Enxame de Partículas}
  Na metaheurística Enxame de Partículas temos um conjunto de agentes (as
  partículas) percorrendo o espaço de soluções. O ``espaço'' onde uma partícula
  está é a solução por ela encontrada.
  \newline\newline
  Cada partícula tem, além do espaço, uma característica chamada ``velocidade''
  que indica como a busca (ou seja, o ``deslocamento'' de um espaço para outro)
  será realizada.
\end{frame}

\begin{frame}{Enxame de Partículas}
  Na metaheurística tanto a melhor solução encontrada pela partícula como as
  soluções encontradas por seus vizinhos e também a melhor solução encontrada
  por todo o enxame influenciará na busca.
\end{frame}

\begin{frame}{Enxame de Partículas}
  A proposta inicial do PSO foi voltada para resolução de problemas de
  otimização contínuos, onde operações de adição e/ou subtração são possíveis na
  solução.
  \newline\newline
  Apenas recentemente foram publicados estudos de diferentes autores para
  registrar o esforço de utilizar PSO em problemas de otimização combinatória.
\end{frame}

\begin{frame}{Enxame de Partículas}
  Como o foco da disciplina são problemas de otimização combinatória, apenas os
  operadores para PSO nesse tipo de problema serão apresentados.
\end{frame}

\section{Etapas do Enxame de Partículas}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Etapas do Enxame de Partículas}
  Detalharemos a seguir as principais etapas do PSO:
  \begin{itemize}
   \item Representação do Espaço;
   \item Representação da Velocidade;
   \item Geração do Enxame;
   \item Avaliação das Partículas;
   \item Atualização da Velocidade;
   \item Deslocamento;
   \item Critério de parada.
  \end{itemize}
\end{frame}

\begin{frame}{Representação do Espaço}
  \textbf{Espaço} é como referenciamos uma solução no PSO. Ele é um vetor com a
  combinação de valores das variáveis de decisão que representa alguma solução
  para o problema.
  \newline\newline
  Tanto soluções viáveis quanto inviáveis podem ser representadas no PSO, desde
  que as soluções inviáveis tenham alguma penalidade ou sejam tratadas como
  não-respostas para o problema aplicado.
\end{frame}

\begin{frame}{Representação do Espaço}
  Por exemplo, uma solução para o Problema da Mochila 0-1 com 5 itens poderia
  ter uma representação do espaço no PSO dada por:
  \begin{center}
   $x$ = [1, 0, 1, 1, 0]
  \end{center}
\end{frame}

\begin{frame}{Representação da Velocidade}
  \textbf{Velocidade} indica a maneira pela qual o PSO fará a busca no espaço de
  busca -- ou seja, como ele se ``deslocará'', partindo de uma solução $x_t$
  para uma solução $x_{t + 1}$.
  \newline\newline
  A \textbf{Velocidade} também é representada por um vetor que deve informar de
  que maneira a busca (ou seja, a alteração de valores na representação do
  espaço) é feita.
\end{frame}

\begin{frame}{Representação da Velocidade}
  Por exemplo, para o Problema da Mochila 0-1 com 5 itens, poderíamos ter o
  vetor \textbf{Velocidade} representando os itens que devem ter seus valores
  alterados no Espaço.
  \newline\newline
  No exemplo abaixo, os itens 3 e 5 teriam seus valores permutados para outros
  possíveis na busca.
  \begin{center}
   $v$ = [3, 5]
  \end{center}
\end{frame}

\begin{frame}{Geração do Enxame}
  No início do método é necessário gerar o Enxame de Partículas, ou seja, todas
  as partículas que farão as buscas do PSO.
  \newline\newline
  No geral essa geração é feita de forma aleatória, a não ser que partes da
  instância aplicada sejam conhecidas o que pode auxiliar na geração dessas
  partículas.
  \newline\newline
  No caso, a geração atua tanto sobre os Espaços quanto sobre as Velocidades. A
  Velocidade das partículas pode ser configurada para um vetor aleatório pequeno
  ou mesmo 0/vetor vazio no início.
\end{frame}

\begin{frame}{Avaliação das Partículas}
  Após a geração do enxame, as partículas precisam ser avaliadas para configurar
  alguns parâmetros e assim utilizá-los na parte de busca do método.
  \newline\newline
  Para todas as partículas, é guardado o melhor espaço que cada uma encontrou
  durante a busca. Esse espaço será salvo em $z_l$.  
  \newline\newline
  Também é verificado qual delas representa a melhor solução do enxame para o
  momento. Esse espaço será salvo na variável $z_g$.
\end{frame}

\begin{frame}{Atualização da Velocidade}
  Com as partículas criadas, inicia-se o processo de atualização da Velocidade
  de cada partícula. Essa atualização é dada pela seguinte equação:
  \begin{center}
   $v_{t + 1}^i = c_1 v_t^i + c_2 \rho_1 (z_l^i - x_t^i) + c_3 \rho_2 (z_v - x_t^i)
   + c_4 \rho_3 (z_g - x_t^i)$
  \end{center}
  Onde $v_t^i$ e $v_{t + 1}^i$ são respectivamente as velocidades atuais e novas
  da partícula $i$; $x_t^i$ é o espaço atual da partícula $i$; $z_l^i$ é o
  melhor espaço já encontrado pela partícula $i$; $z_v$ é o melhor espaço
  encontrado por um subconjunto de vizinhos; $z_g$ é o melhor espaço encontrado
  por qualquer partícula.
  \newline\newline
  $c_i; i = 1, 2, 3, 4$ são coeficientes que representam o peso de cada
  componente na definição da nova velocidade e $\rho_i; i = 1, 2, 3$ são números
  randômicos entre 0 e 1.
\end{frame}

\begin{frame}{Atualização da Velocidade}
  As operações na Atualização das Velocidades deve refletir algum tipo de
  alteração possível no espaço codificado pela partícula.
  \newline\newline
  Por exemplo, a subtração de espaços pode ser os bits diferentes entre dois
  espaços para o Problema da Mochila 0-1. Assim, para os seguintes espaços:
  \begin{center}
   $e_1$ = [1, 0, 1, 1, 0]\\
   $e_2$ = [1, 0, 0, 0, 0]
  \end{center}
  Teríamos que $e_1 - e_2$ poderia ser representador por [3, 4].
\end{frame}

\begin{frame}{Atualização da Velocidade}
  Ainda para o exemplo do Problema da Mochila 0-1, a soma poderia representar a
  concatenação de vetores. Assim, dado:
  \begin{center}
   $v_1$ = [1]\\
   $v_2$ = [3, 4]
  \end{center}
  Teríamos $v_1 + v_2$ correspondente a [1, 3, 4].
\end{frame}

\begin{frame}{Atualização da Velocidade}
  Já a multiplicação, também para o exemplo, poderia codificar que, caso o
  resultado fosse menor tivesse casas decimais menores que 0,5, o resultado
  seria arredondado para baixo e, caso iguais ou acima de 0,5, arredondado para
  cima.
  \newline\newline
  Dessa forma, 0,75 * [1, 3, 4] resultaria em [0,75, 2,25, 3], que convertidos
  teríamos [1, 2, 3].
\end{frame}

\begin{frame}{Atualização da Velocidade}
  As ideias apresentadas para as operações servem apenas como ilustrações para o
  Problema da Mochila 0-1, e devem ser cuidadosamente trabalhadas quando da
  implementação objetivando outros tipos de problemas combinatoriais.
\end{frame}

\begin{frame}{Atualização da Velocidade}
  A variável $z_v$ diz respeito ao melhor espaço de um subconjunto de soluções
  ``vizinhas'' $v$. O valor de $v$ é obtido de forma aleatória a cada iteração.
  \newline\newline
  Esse componente da equação relaciona-se com a influência que vizinhos possam
  ter na partícula durante o movimento das mesmas na busca por soluções para o
  problema.
\end{frame}

\begin{frame}{Atualização da Velocidade}
  Os coeficientes $c_i; i = 1, 2, 3, 4$ indicam o peso dado a cada componente da
  equação.
  \newline\newline
  No geral, $c_1$ é maior que 0 e menor que 1, identificando a porcentagem da
  velocidade que será reduzida.
  \newline\newline
  Em implementações modernas do PSO, $c_4$ é configurado para 0, eliminando a
  influência que a melhor solução encontrada até o momento tenha sobre as demais
  partículas do enxame.
\end{frame}

\begin{frame}{Deslocamento}
  Obtida a nova velocidade $v_{t + 1}^i$ para a partícula $i$, chega o momento
  de calcular o \textbf{Deslocamento} que essa partícula executará, ou seja,
  para onde ela caminhará e qual será o novo espaço ($x_{t + 1}^i$) ocupado por
  ela.
  \newline\newline
  A equação do deslocamento é dada por:
  \begin{center}
   $x_{t + 1}^i = x_t^i \circ v_{t + 1}^i$
  \end{center}
  Essa equação representa a aplicação da velocidade $v_{t + 1}^i$ no espaço
  $x_t^i$.
\end{frame}

\begin{frame}{Deslocamento}
  Por exemplo, dado que para uma partícula $i$ tenha-se o espaço $x_t^i$ e a
  velocidade $v_{t + 1}^i$ abaixo:
  \begin{center}
   $x_t^i$ = [1, 0, 1, 1, 0]\\
   $v_{t + 1}^i$ = [1, 2, 3]
  \end{center}
  A aplicação de $v_{t + 1}^i$ em $x_t^i$ deslocará a partícula $i$ para o
  seguinte espaço $x_{t + 1}^i$:
  \begin{center}
   $x_{t + 1}^i$ = [0, 1, 0, 1, 0]
  \end{center}
\end{frame}

\begin{frame}{Critério de Parada}
  Com o Deslocamento de todas as partículas realizado, o PSO retorna à etapa de
  Avaliação das Partículas para verificar os melhores espaços encontrados e
  reinicializa o cálculo da atualização da velocidade e deslocamento.
  \newline\newline
  O método realizará essas iterações até atingir um critério de parada, que pode
  ser um número máximo de iterações ou um certo número de iterações onde não
  houve mudança na melhor solução encontrada ou mudança significativa na média
  global de espaços encontrados em cada iteração (quando normalmente o método
  convergiu).
\end{frame}

\section{Pseudocódigo}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Pseudocódigo}
  Os principais parâmetros para o PSO que precisam ser decididos  durante o
  projeto do algoritmo são:
  \begin{itemize}
   \item O número de partículas a serem geradas;
   \item A representação do Espaço;
   \item A representação da Velocidade;
   \item O tamanho da vizinhança $v$ para obter $z_v$;
   \item Os pesos para os coeficientes $c_i; i = 1, 2, 3, 4$;
   \item Critério de parada.
  \end{itemize}
  A seguir temos o pseudocódigo para o Enxame de Partículas.
\end{frame}

\begin{frame}[fragile]{Pseudocódigo}  
  \begin{algorithmic}
   \STATE Cria o Enxame de Partículas
   \REPEAT
      \STATE Avalia as Partículas
      \STATE Atualiza as Velocidades
      \STATE Realiza o Deslocamento das Partículas
   \UNTIL{Atingir critério de parada}
  \end{algorithmic}
\end{frame}

\section{Conclusões}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Conclusões}
 \begin{itemize}
  \item Enxame de Partículas é uma metaheurística populacional baseada no
  comportamento social que um conjunto de animas desempenham, como a
  movimentação de um cardume de peixes ou revoada de pássaros;
  \item A busca é realizada por cada partícula se deslocando no espaço de
  soluções;
  \item Cada partícula assume um Espaço (solução) e se move a partir de uma
  Velocidade, calculada para cada partícula a cada iteração;
  \item Com o decorrer das iterações, espera-se que o método tenha convergido
  para alguma boa solução no espaço de busca.
 \end{itemize}
\end{frame}

\begin{frame}
    \maketitle
\end{frame}

\end{document}
