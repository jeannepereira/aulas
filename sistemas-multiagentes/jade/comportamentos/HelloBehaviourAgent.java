import jade.core.Agent;

public class HelloBehaviourAgent extends Agent{
  protected void setup(){
    addBehaviour(new HelloBehaviour(this));
  }
}
