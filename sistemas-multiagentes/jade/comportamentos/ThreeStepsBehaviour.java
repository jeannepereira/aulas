import jade.core.behaviours.Behaviour;

public class ThreeStepsBehaviour
  extends Behaviour {

  private int step = 0;

  public void action() {
    switch(step):
      case 0:
        // tarefas de 0
        step++;
        break;
      case 1:
        // tarefas de 1
        step++;
        break;
      case 2:
        // tarefas de 2
        step++;
        break;
      case 3:
        // tarefas de 3
        step++;
        break;
  }

  public boolean done() {
    return step == 3;
  }
}
