\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage[brazilian]{babel}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{listings}
\usepackage{amsmath}
\usepackage[scaled=.95]{helvet}% helvetica as the origin of arial
\usepackage[helvet]{sfmath}    % for the mathematical enviroments
\renewcommand{\familydefault}{\sfdefault}

\lstset{showstringspaces=false}
%% ETH beamer theme
% Options: [default]
%   itemsblack/[itemsblue]: change color of bullets etc. to black/blue in
%   itemize style environments
%   [titlesblack]/titlesblue: change color of frame titles/subtitles to
%   black/blue
% \usetheme[itemsblack,titlesblack]{eth}
\usetheme{eth}

%% Theme uses ETH blue color by default. Can be changed to any color using this
%% command: 
\setbeamercolor{structure}{fg=ETHred}

%% Mandatory variables
\author{Filipe Saraiva}
\title{JADE -- Comportamentos (Behaviours)}
\subtitle{Sistemas Multiagentes}
\institute{UFPA}
\date{\today}

\begin{document}

\begin{frame}
    \maketitle
\end{frame}

\begin{frame}{Conteúdo}
	\tableofcontents
\end{frame}

\section{Introdução}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Introdução}
 Nesse módulo serão apresentadas as maneiras como agentes JADE realizam
 tarefas.
 \newline\newline
 Nesse \textit{framework} são utilizados os conceitos de \textbf{Comportamentos}
 -- \textit{Behaviours} no original, que servem como metáfora para a maneira
 pela qual agentes desenvolvem tarefas.
\end{frame}

\section{Comportamentos}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\subsection{Conceito}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsection]
\end{frame}

\begin{frame}{Conceito}
 Existem vários tipos arquiteturais de agentes e sistemas multiagentes. Dentre
 eles, um bastante estudado e desenvolvido é o \textbf{Agente Reativo}.
 \newline\newline
 \textbf{Agente Reativo} é um tipo de arquitetura onde o agente tem uma
 representação simples do mundo em que está modelado, porém provendo um bom
 mapeamento e interação com ele a partir dos sensores e atuadores.
\end{frame}

\begin{frame}{Conceito}
 Nesse tipo de agente é comum que a programação seja realizada baseada em
 \textbf{Comportamentos}, que são ações que se espera de um agente caso ele
 receba sinais externos, seja do  ambiente ou de outros agentes.
 \newline\newline
 Nesse sentido, a inteligência em sistemas multiagentes reativos advém da
 interação que esses agentes realizam a partir de seus comportamentos.
\end{frame}

\begin{frame}{Conceito}
 Em JADE, os comportamentos são classes Java que herdam a classe
 \textit{Behaviour} (ou alguma das classes especializadas). Essas classes são
 então adicionadas ao agente no método \textit{setup()} do agente ou a partir de
 algum outro comportamento.
 \newline\newline
 Em ambos os casos, utiliza-se o método \textit{addBehaviour(behaviour)} do
 agente, passando o comportamento como argumento.
\end{frame}

\begin{frame}{Conceito}
 Cada \textit{Behaviour} tem 2 métodos abstratos que devem ser implementados.
 \begin{itemize}
  \item \textit{action()} define a operação realizada pelo agente. Sempre que um
  \textit{Behaviour} é executado, ele executa o método \textit{action()};
  \item \textit{done()} retorna um booleano para indicar que um comportamento
  foi finalizado. Quando verdadeiro, também remove o comportamento da base de
  comportamentos do agente.
 \end{itemize}
\end{frame}

\begin{frame}{Conceito}
 Quando um comportamento é executado em JADE, o método \textit{action()}
 correspondente roda até o final. Chegando ao final, o agente executa o método
 \textit{done()}, onde o critério de parada do comportamento é verificado.
\end{frame}

\begin{frame}{Conceito}
 Exemplificando, uma classe que implementa um ``Hello Behaviour'' teria essa
 forma:
 \lstinputlisting[language=java]{HelloBehaviour.java}
\end{frame}

\begin{frame}{Conceito}
 Para adicionar essa classe ao agente, faríamos o seguinte:
 \lstinputlisting[language=java]{HelloBehaviourAgent.java}
\end{frame}

\begin{frame}{Conceito}
 JADE já traz diversos comportamentos pré-definidos, que são comportamentos
 cujos métodos \texttt{done()} já estão implementados de maneiras diferentes.
 Esses comportamentos são:
 \begin{itemize}
  \item \textit{One-Shot Behaviour};
  \item \textit{Cyclic Behaviour};
  \item \textit{Generic Behaviour};
  \item \textit{Waker Behaviour};
  \item \textit{Ticker Behaviour}.
 \end{itemize}
\end{frame}

\subsection{One-Shot Behaviour}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsection]
\end{frame}

\begin{frame}{One-Shot Behaviour}
 \textit{One-Shot Behaviour} são comportamentos que acontecem apenas 1 vez. Após
 o método \texttt{action()} ser executado, a execução do método \texttt{done()}
 retornará \texttt{true} em seguida.
 \newline\newline
 Esse comportamento é conveniente para ações que são tomadas uma única vez. Cabe
 ressaltar que após isso o comportamento é removido da base de comportamentos
 dos agentes, sendo necessário readicioná-lo para uma nova execução.
\end{frame}

\begin{frame}{One-Shot Behaviour}
 Um código típico desse comportamento seria:
 \lstinputlisting[language=java]{OneTimeBehaviour.java}
\end{frame}

\subsection{Cyclic Behaviour}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsection]
\end{frame}

\begin{frame}{Cyclic Behaviour}
 \textit{Cyclic Behaviour} são comportamentos que nunca param de serem
 executados. Isso significa que o método \texttt{done()} sempre retorna
 \texttt{false}.
 \newline\newline
 Esse comportamento é utilizado para ações que necessitam que o agente não pare
 de realizar o comportamento. Um dos exemplos mais comuns desse tipo de situação
 é sobre o agente que está sempre atento aos sinais do ambiente ou aqueles
 agentes que são puramente reativos, esperando mensagens de outros agentes.
\end{frame}

\begin{frame}{Cyclic Behaviour}
 Um código típico desse comportamento seria:
 \lstinputlisting[language=java]{LoopBehaviour.java}
\end{frame}

\subsection{Generic Behaviour}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsection]
\end{frame}

\begin{frame}{Generic Behaviour}
 \textit{Generic Behaviour} são comportamentos que devem implementar o método
 \texttt{done()} de forma a definir como é a finalização do comportamento.
 \newline\newline
 É comum que esse comportamento realize diferentes tarefas a partir de uma
 variável que define o estado interno do agente.
\end{frame}

\begin{frame}{Generic Behaviour}
 Um código típico desse comportamento seria:
 \lstinputlisting[language=java]{ThreeStepsBehaviour-1.java}
\end{frame}

\begin{frame}{Generic Behaviour}
 Um código típico desse comportamento seria (continua):
 \lstinputlisting[language=java]{ThreeStepsBehaviour-2.java}
\end{frame}

\begin{frame}{Generic Behaviour}
 Um código típico desse comportamento seria (continua):
 \lstinputlisting[language=java]{ThreeStepsBehaviour-3.java}
\end{frame}

\subsection{Waker Behaviour}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsection]
\end{frame}

\begin{frame}{Waker Behaviour}
 \textit{Waker Behaviour} é um comportamento que ocorre 1 vez após um
 determinado tempo especificado no construtor do comportamento.
 \newline\newline
 O construtor do comportamento recebe como segundo argumento o tempo de espera,
 dado em milisegundos (1s = 1000ms).
 \newline\newline
 As ações que realizam esse comportamento devem ser implementadas na função
 \texttt{onWake()}, que é abstrata na classe original.
\end{frame}

\begin{frame}{Waker Behaviour}
 Um código típico desse comportamento seria:
 \lstinputlisting[language=java]{HiTimeBehaviour.java}
\end{frame}

\section{Conclusões}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Conclusões}
\begin{itemize}
 \item JADE 
\end{itemize}
\end{frame}

\begin{frame}
    \maketitle
\end{frame}

\end{document}
