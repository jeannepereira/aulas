import jade.core.behaviours.OneShotBehaviour;

public class OneTimeBehaviour
    extends OneShotBehaviour {

  public void action() {
    System.out.println("Just one time!")
  }

}
