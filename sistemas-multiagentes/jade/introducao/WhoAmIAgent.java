import jade.core.Agent;

public class WhoAmIAgent extends Agent{
    protected void setup(){
        System.out.println("Hello World," +
            " I am an agent!");
        System.out.println("My local name: " +
            this.getAID().getLocalName());
        System.out.println("My global name: " +
            this.getAID().getName());
    }
}
