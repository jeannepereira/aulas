import jade.core.Agent;
import jade.core.behaviours.ParallelBehaviour;
import jade.core.behaviours.CyclicBehaviour;

public class ParallelAgent extends Agent{
    protected void setup(){
        ParallelBehaviour pb = new ParallelBehaviour(this, ParallelBehaviour.WHEN_ALL);

        pb.addSubBehaviour(new CyclicBehaviour(this){
            public void action(){
                System.out.println("First behaviour");
            }
        });

        pb.addSubBehaviour(new CyclicBehaviour(this){
            public void action(){
                System.out.println("Second behaviour");
            }
        });

        addBehaviour(pb);
    }
}
