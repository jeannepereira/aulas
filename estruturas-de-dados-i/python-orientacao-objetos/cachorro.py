class Cachorro:
    def __init__(self, nome):
        self.nome = nome

    def latir(self):
        print('Au au!!!')

    def correr(self):
        print('Correndo!')

    def nomeCachorro(self):
        print(self.nome)
