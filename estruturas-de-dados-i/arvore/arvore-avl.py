#
#    Implementação de Árvore AVL em Python 3
#    Copyright (C) 2017, Filipe Saraiva <saraiva@ufpa.br>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

#
# Classe Nodo
#
# Implementa um Nodo, uma classe simples que guarda qualquer informação
#
# Essa classe será utilizada na árvore bináriaria de pesquisa. Ela contém um
# atributo para a informação (info), outro para indicar o nodo pai (nodoPai), e
# dois ponteiros para indicar o nó filho da esquerda e o nó filho da direita
#

# Definição da classe Nodo
class Nodo:
    
    #Método construtor
    def __init__(self, info):
        
        # O argumento 'info' representa uma infomação
        # qualquer a ser aplicada na classe Nodo
        self.info = info
        
        # O atributo nodoPai guarda o nodo exatamente anterior (um nível acima)
        # a qual o nodo criado se relaciona
        self.nodoPai = None
        
        # Ponteiro para o nodo filho da esquerda, que para a árvore binária de
        # pesquisa sempre terá a informação menor que a do nodo atual
        self.nodoFilhoEsquerda = None
        
        # Ponteiro para o nodo filho da direita, que para a árvore binária de
        # pesquisa sempre terá a informação maior que a do nodo atual
        self.nodoFilhoDireita = None
        
        # Fator de balanceamento, utilizado para verificar se as subárvores da
        # árvore relacionada ao respectivo nó estão desbalanceadas ou não
        self.fatorBalanceamento = 0
        

#
# Classe Árvore AVL
#
# Uma das principais vantagens da Árvore Binária de Busca é que essa estrutura
# serve como um guia para realização de buscas por elementos. Entretanto, ainda
# é possível otimizar essas buscas se mantermos a árvore balanceada, reduzindo
# o número de iterações para se encontrar um elemento. Para esses casos
# trabalhamos com a Árvore AVL, que é binária e balanceada, ou seja, as
# subárvores tanto da direita quanto da esquerda tem tamanhos próximos (irão
# diferir em no máximo 1 nível entre elas.
#
# Na Árvore AVL, a inserção de um nodo funcionará como na Árvore Binária de
# Pesquisa, entretanto quando alguma inserção desbalancear em demasia a árvore
# uma série de alterações chamadas "rotações" serão acionadas para balancear a
# árvore novamente.
#
# Esta classe implementa uma Árvore AVL com alocação encadeada em Python 3. Os
# métodos implementados são de inserção geral, busca e impressão. Maiores
# detalhes sobre cada método estão em suas respectivas descrições.
#

# Definição da classe ArvoreAVL
class ArvoreAVL:
    
    # Método construtor
    def __init__(self):
        
        # Ponteiro para o primeiro nodo raiz da árvore
        # do qual todos os demais derivarão
        self.raiz = None
    
    # Função para inserção de nodos na árvore
    # Se a árvore não tiver um nodo raiz, o primeiro nodo a ser inserido será a
    # raiz e a ele caberá particionar a árvore entre menores/maiores nodos que
    # forem inseridos posteriormente.
    #
    # A implementação é realizada de maneira recursiva para que o nodo seja
    # inserido no local correto da árvore, seguindo as relações existentes na
    # árvore binária de pesquisa
    #
    # Durante a inserção, os fatores de balanceamento dos nodos antecessores
    # serão recalculados. Caso algum dele resulte em -2 ou 2, um algoritmo de
    # rotação será executado, reorganizando os nodos da árvore. Explicações
    # sobre eles são dadas adiante.
    #
    # O cálculo do fator de balanceamento é o nível da subárvore da direita
    # subtraído do nível da subárvore da esquerda.
    #
    # nodo - nodo a ser inserido na árvore
    # nodoPai - nodo que será o pai do nodo a ser inserido, utilizado apenas
    # internamente pela função
    def insereNodo(self, nodo, nodoPai = ''):
        
        # Se não há raiz, então o nodo a ser
        # inserido será a raiz da árvore
        if not self.raiz:
            self.raiz = nodo
            return
        
        # Caso o nodoPai não tenha sido definido,
        # será utilizado a raiz da árvore
        if nodoPai == '':
            nodoPai = self.raiz
        
        # Nessa instrução verifica-se se o nodo a ser inserido tem uma info
        # menor que a info do nodoPai - caso sim, ele terá que ser inserido
        # na esquerda do nodo pai
        if nodo.info < nodoPai.info:
            
            # Se o nodoPai tem o nodoFilhoEsquerda vazio, então pode-se inserir
            # o nodo diretamente no espaço, criando as relações entre ponteiros
            # nos nodos
            if nodoPai.nodoFilhoEsquerda == None:
                nodo.nodoPai = nodoPai
                nodoPai.nodoFilhoEsquerda = nodo
                
                # Verificação para cálculo dos fatores de balanceamento dos
                # antecessores - caso o nó inserido na esquerda não tenha um
                # irmão na direita, o nível da subárvore aumentou e portanto
                # o cálculo deverá ser executado
                if nodoPai.nodoFilhoDireita == None:
                    nodoCalculaFatorBalanceamento = nodo
                    
                    # O laço while irá percorrer os antecessores do nodo
                    # inserido até a raiz, recalculando os fatores de
                    # balanceamento, ou até que algum fator resulte em -2 ou 2
                    while nodoCalculaFatorBalanceamento != self.raiz:
                        
                        # Cálculo do fator de balanceamento
                        if nodoCalculaFatorBalanceamento.nodoPai.nodoFilhoDireita == nodoCalculaFatorBalanceamento:
                            nodoCalculaFatorBalanceamento.nodoPai.fatorBalanceamento += 1
                        else:
                            nodoCalculaFatorBalanceamento.nodoPai.fatorBalanceamento -= 1
                        
                        # Se o fator de balanceamento resultou em -2, a árvore
                        # necessita de uma rotação à direita. Após a rotação não
                        # é necessário continuar com o cálculo do fator de
                        # balanceamento dos antecessores.
                        if nodoCalculaFatorBalanceamento.nodoPai.fatorBalanceamento == -2:
                            self.rotacaoDireita(nodoCalculaFatorBalanceamento)
                            break
                        
                        # Se o fator de balanceamento resultou em 2, a árvore
                        # necessita de uma rotação à esquerda. Após a rotação
                        # não é necessário continuar com o cálculo do fator de
                        # balanceamento dos antecessores.
                        if nodoCalculaFatorBalanceamento.nodoPai.fatorBalanceamento == 2:
                            self.rotacaoEsquerda(nodoCalculaFatorBalanceamento)
                            break
            
                        nodoCalculaFatorBalanceamento = nodoCalculaFatorBalanceamento.nodoPai
                
                # Se houver um irmão, apenas atualiza o fator de balanceamento
                # no nodo pai
                else:
                    nodoPai.fatorBalanceamento -= 1
                        
                return
            
            # Caso contrário, chama-se recursivamente a função passando o nodo
            # e o nodoFilhoEsquerda do nodo pai para avaliar se o nodo a ser
            # inserido ficará na esquerda ou na direita do nodoFilhoEsquerda
            else:
                self.insereNodo(nodo, nodoPai.nodoFilhoEsquerda)
        
        # Caso o nodo a ser inserido tenha a info maior que a do nodoPai, então
        # ele deve ficar à direita deste
        if nodo.info > nodoPai.info:
            
            # Se o nodoFilhoDireita estiver vazio, aloca-se o nodo lá
            if nodoPai.nodoFilhoDireita == None:
                nodo.nodoPai = nodoPai
                nodoPai.nodoFilhoDireita = nodo
                
                # Verificação para cálculo dos fatores de balanceamento dos
                # antecessores - caso o nó inserido na direita não tenha um
                # irmão na esquerda, o nível da subárvore aumentou e portanto
                # o cálculo deverá ser executado
                if nodoPai.nodoFilhoEsquerda == None:
                    nodoCalculaFatorBalanceamento = nodo
                    
                    # O laço while irá percorrer os antecessores do nodo
                    # inserido até a raiz, recalculando os fatores de
                    # balanceamento, ou até que algum fator resulte em -2 ou 2
                    while nodoCalculaFatorBalanceamento != self.raiz:
                        
                        # Cálculo do fator de balanceamento
                        if nodoCalculaFatorBalanceamento.nodoPai.nodoFilhoDireita == nodoCalculaFatorBalanceamento:
                            nodoCalculaFatorBalanceamento.nodoPai.fatorBalanceamento += 1
                        else:
                            nodoCalculaFatorBalanceamento.nodoPai.fatorBalanceamento -= 1
            
                        # Se o fator de balanceamento resultou em -2, a árvore
                        # necessita de uma rotação à direita. Após a rotação não
                        # é necessário continuar com o cálculo do fator de
                        # balanceamento dos antecessores.
                        if nodoCalculaFatorBalanceamento.nodoPai.fatorBalanceamento == -2:
                            self.rotacaoDireita(nodoCalculaFatorBalanceamento)
                            break
                        
                        # Se o fator de balanceamento resultou em 2, a árvore
                        # necessita de uma rotação à esquerda. Após a rotação
                        # não é necessário continuar com o cálculo do fator de
                        # balanceamento dos antecessores.
                        if nodoCalculaFatorBalanceamento.nodoPai.fatorBalanceamento == 2:
                            self.rotacaoEsquerda(nodoCalculaFatorBalanceamento)
                            break
                        
                        nodoCalculaFatorBalanceamento = nodoCalculaFatorBalanceamento.nodoPai
                
                # Se houver um irmão, apenas atualiza o fator de balanceamento
                # no nodo pai
                else:
                    nodoPai.fatorBalanceamento += 1
                        
                return
            
            # Caso contrário, chamada recursiva para avaliar se esse nodo ficará
            # à esquerda ou à direita do nodoFilhoDireita
            else:
                self.insereNodo(nodo, nodoPai.nodoFilhoDireita)
        
        return
    
    # Rotação à direita dos nodos da árvore, ativado
    # quando o desbalancemanto atinge o valor -2
    #
    # nodo - o nodo de onde partiu a necessidade de rotação
    def rotacaoDireita(self, nodo):
        # Simples
        if nodo.fatorBalanceamento == -1:
            
            # Atualiza as ligações do pai do nodo pai
            # para o nodo que será rotacionado
            if nodo.nodoPai.nodoPai != None:
                if nodo.nodoPai.nodoPai.nodoFilhoDireita == nodo.nodoPai:
                    nodo.nodoPai.nodoPai.nodoFilhoDireita = nodo
                else:
                    nodo.nodoPai.nodoPai.nodoFilhoEsquerda = nodo
            
            # Caso seja None, o nodo a ser rotacionado
            # será a nova raiz da árvore
            else:
                nodo.nodoPai.nodoPai = nodo
                self.raiz = nodo
            
            # Se o nodo a ser rotacionado tem uma subárvore na
            # direita, ela passará a ser a subárvore da esquerda
            # do nodo pai do que será rotacionado
            if nodo.nodoFilhoDireita:
                nodo.nodoPai.nodoFilhoEsquerda = nodo.nodoFilhoDireita
                
            nodo.nodoFilhoDireita = nodo.nodoPai
            
            # Zera os balanceamentos dos nodos rotacionados
            nodo.fatorBalanceamento = 0
            nodo.nodoPai.fatorBalanceamento = 0
            
            if self.raiz == nodo:
                nodo.nodoPai = None
        
        # Dupla
        if nodo.fatorBalanceamento == 1:
            
            nodoSubstituto = nodo.nodoFilhoDireita
            
            # Zera os balanceamentos dos nodos a serem rotacionados
            nodoSubstituto.fatorBalanceamento = 0
            nodo.fatorBalanceamento = 0
            nodo.nodoPai.fatorBalanceamento = 0
            
            # Atualiza as ligações do pai do nodo pai
            # para o nodo que será rotacionado
            if nodo.nodoPai != self.raiz:
                nodoSubstituto.nodoPai = nodo.nodoPai.nodoPai
                if nodo.nodoPai.nodoPai.nodoFilhoEsquerda == nodo.nodoPai:
                    nodo.nodoPai.nodoPai.nodoFilhoEsquerda = nodoSubstituto
                else:
                    nodo.nodoPai.nodoPai.nodoFilhoDireita = nodoSubstituto
            
            # Caso seja None, o nodo a ser rotacionado
            # será a nova raiz da árvore
            else:
                nodoSubstituto.nodoPai = None
                self.raiz = nodoSubstituto
            
            nodo.nodoFilhoDireita = None
            nodo.nodoPai.nodoFilhoEsquerda = None
            
            nodoSubstituto.nodoFilhoDireita = nodo.nodoPai
            nodo.nodoPai.nodoPai = nodoSubstituto
            
            nodoSubstituto.nodoFilhoEsquerda = nodo
            nodo.nodoPai = nodoSubstituto
            
        return
    
    # Rotação à esquerda dos nodos da árvore, ativado
    # quando o desbalancemanto atinge o valor 2
    #
    # nodo - o nodo de onde partiu a necessidade de rotação
    def rotacaoEsquerda(self, nodo):
        # Simples
        if nodo.fatorBalanceamento == 1:
            
            # Atualiza as ligações do pai do nodo pai
            # para o nodo que será rotacionado
            if nodo.nodoPai.nodoPai != None:
                if nodo.nodoPai.nodoPai.nodoFilhoDireita == nodo.nodoPai:
                    nodo.nodoPai.nodoPai.nodoFilhoDireita = nodo
                else:
                    nodo.nodoPai.nodoPai.nodoFilhoEsquerda = nodo
            
            # Caso seja None, o nodo a ser rotacionado
            # será a nova raiz da árvore
            else:
                nodo.nodoPai.nodoPai = nodo
                self.raiz = nodo
            
            # Se o nodo a ser rotacionado tem uma subárvore na
            # esquerda, ela passará a ser a subárvore da direita
            # do nodo pai do que será rotacionado
            if nodo.nodoFilhoEsquerda:
                nodo.nodoPai.nodoFilhoDireita = nodo.nodoFilhoEsquerda
                
            nodo.nodoFilhoEsquerda = nodo.nodoPai
            
            # Zera os balanceamentos dos nodos rotacionados
            nodo.fatorBalanceamento = 0
            nodo.nodoPai.fatorBalanceamento = 0
            
            if self.raiz == nodo:
                nodo.nodoPai = None
        
        # Dupla
        if nodo.fatorBalanceamento == -1:
            
            nodoSubstituto = nodo.nodoFilhoEsquerda
            
            # Zera os balanceamentos dos nodos rotacionados
            nodoSubstituto.fatorBalanceamento = 0
            nodo.fatorBalanceamento = 0
            nodo.nodoPai.fatorBalanceamento = 0
            
            # Atualiza as ligações do pai do nodo pai
            # para o nodo que será rotacionado
            if nodo.nodoPai != self.raiz:
                nodoSubstituto.nodoPai = nodo.nodoPai.nodoPai
                if nodo.nodoPai.nodoPai.nodoFilhoEsquerda == nodo.nodoPai:
                    nodo.nodoPai.nodoPai.nodoFilhoEsquerda = nodoSubstituto
                else:
                    nodo.nodoPai.nodoPai.nodoFilhoDireita = nodoSubstituto
            
            # Caso seja None, o nodo a ser rotacionado
            # será a nova raiz da árvore
            else:
                nodoSubstituto.nodoPai = None
                self.raiz = nodoSubstituto
            
            nodo.nodoFilhoEsquerda = None
            nodo.nodoPai.nodoFilhoDireita = None
            
            nodoSubstituto.nodoFilhoEsquerda = nodo.nodoPai
            nodo.nodoPai.nodoPai = nodoSubstituto
            
            nodoSubstituto.nodoFilhoDireita = nodo
            nodo.nodoPai = nodoSubstituto
        
        return
    
    # Função para buscar uma informação na árvore
    # A implementação é feita de maneira recursiva.
    #
    # info - informação a ser buscada na árvore
    # nodo - 'nodo base' da função, utilizado apenas nas chamadas recursivas
    def buscaInfo(self, info, nodo = ''):
        
        # resultado aqui funciona como uma variável para checar se a informação
        # foi encontrada ou não
        resultado = False
        
        # Na primeira chamada da função, nodo passará a ser a raiz da árvore
        if nodo == '':
            nodo = self.raiz
            
        # Verifica-se se o nodo já tem a informação buscada. Caso sim,
        # suspende-se a busca nesse ponto.
        if nodo.info == info:
            return True
        # Caso não, verifica-se se a informação no nodo é maior que a buscada
        # e se há árvore à esquerda - caso sim, avalia-se essa subárvore a
        # partir de uma chamada recursiva para o nodoFilhoEsquerda
        elif nodo.info > info and nodo.nodoFilhoEsquerda:
            resultado = self.buscaInfo(info, nodo.nodoFilhoEsquerda)
        # Para o caso onde a informação no nodo é menor que a buscada,
        # verifica-se se há árvore à direita e avalia-se essa subárvore a
        # partir de uma chamada recursiva para o nodoFilhoDireita
        elif nodo.info < info and nodo.nodoFilhoDireita:
            resultado = self.buscaInfo(info, nodo.nodoFilhoDireita)
        
        return resultado

    # Função para imprimir o conteúdo de todos os nodos da árvore
    # Aqui é implementada uma busca em profundidade, da esquerda para a direita.
    # A implementação é feita de maneira recursiva.
    def imprimeArvore(self, nodo = ''):
        
        if nodo == '':
            nodo = self.raiz
        
        print(nodo.info, end = ' ')
        
        if nodo.nodoFilhoEsquerda:
            self.imprimeArvore(nodo.nodoFilhoEsquerda)
        
        if nodo.nodoFilhoDireita:
            self.imprimeArvore(nodo.nodoFilhoDireita)
        
        if nodo == self.raiz:
            print()
        
        return
    
    # Função para imprimir o conteúdo dos nodos da árvore e seus fatores de
    # balanceamento. Aqui é implementada uma busca em profundidade, da esquerda
    # para a direita. A implementação é feita de maneira recursiva.
    def imprimeFatorBalanceamento(self, nodo = ''):
        
        if nodo == '':
            nodo = self.raiz
        
        print(nodo.info, "(", nodo.fatorBalanceamento, ")", end = ' ')
        
        if nodo.nodoFilhoEsquerda:
            self.imprimeFatorBalanceamento(nodo.nodoFilhoEsquerda)
        
        if nodo.nodoFilhoDireita:
            self.imprimeFatorBalanceamento(nodo.nodoFilhoDireita)
        
        if nodo == self.raiz:
            print()
        
        return
