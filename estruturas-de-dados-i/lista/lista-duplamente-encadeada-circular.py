#
#    Implementação de Lista Linear Duplamente Encadeada Circular em Python 3
#    Copyright (C) 2017, Filipe Saraiva <saraiva@ufpa.br>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

#
# Classe Nodo
#
# Implementa um Nodo, uma classe simples que guarda qualquer informação, um
# "ponteiro" para o próximo nodo e outro "ponteiro" para o nodo anterior na
# estrutura encadeada
#

# Definição da classe Nodo
class Nodo:
    
    #Método construtor
    def __init__(self, info):
        
        # O argumento 'info' representa uma infomação
        # qualquer a ser aplicada na classe Nodo
        self.info = info
        
        # O atributo 'proximoNodo' servirá como referência para o próximo
        # Nodo a ser atribuído no tipo de alocação encadeada. No início o
        # valor será 'None' pois no momento da criação não há qualquer nodo
        # a ser referenciado
        self.proximoNodo = None
        
        # Na alocação duplamente encadeada o nodo também terá uma referência
        # para o nodo anterior a ele na estrutura. Essa ligação será
        # representada pelo atributo 'anteriorNodo' abaixo
        self.anteriorNodo = None

#
# Classe ListaDuplamenteEncadeadaCircular
#
# Esta classe implementa uma lista duplamente encadeada e circular em Python
# 3. Nesta classe, elementos são relacionados entre si a partir de um
# "ponteiro" que identifica a ordem dos nodos. A lista guarda a referência
# para o primeiro nodo da lista e contém algumas funções para inserção,
# remoção, busca e percorrimento. Não há índices de posição nesse tipo de
# lista. Segue abaixo a implementação da classe bem como seus principais
# métodos, como o construtor, inserção de nodo, remoção e busca
#

# Definição da classe ListaDuplamenteEncadeadaCircular
class ListaDuplamenteEncadeadaCircular:
    
    # Método construtor
    def __init__(self):
        
        # Ponteiro que aponta para o nodo raiz (ou cabeça) da lista. Na
        # inicialização da lista, por não haver nodos, a raiz será None
        self.raiz = None
    
    # Função simples de inserção de nodos no início da lista
    #
    # nodo - o nodo a ser inserido na lista
    def insereNodo(self, nodo):
        # Se não há um nodo raiz já definido, então esse será o nodo raiz
        # da estrutura. Os ponteiros apontam para o próprio nodo pois trata-se
        # de uma lista circular
        if not self.raiz:
            nodo.proximoNodo = nodo
            nodo.anteriorNodo = nodo
            self.raiz = nodo
            
        # Caso já exista uma raiz, os ponteiros do nodo que será adicionado
        # passam a apontar para a raiz e para o anterior da raiz atual. Em
        # seguida atualizam-se os ponteiros próximo nodo do último nodo e
        # o anterior da raiz atual para apontarem para o nodo a ser adicionado.
        # Ao final, o novo nodo será a nova raiz da lista
        else:
            nodo.proximoNodo = self.raiz
            nodo.anteriorNodo = self.raiz.anteriorNodo
            self.raiz.anteriorNodo.proximoNodo = nodo
            self.raiz.anteriorNodo = nodo
            self.raiz = nodo
        
        return
    
    # Remove nodo a partir do conteúdo informado
    #
    # info - conteúdo a ser removido da estrutura
    def removeNodo(self, info):
        # Guarda-se o nodo raiz da estrutura
        nodoAtual = self.raiz
        
        # Guarda-se o "último" nodo da lista circular, que será o nodo anterior
        # da raiz da lista
        ultimoNodo = self.raiz.anteriorNodo
        
        # Essa verificação serve para o caso onde a lista encadeada ainda não
        # tem nodos. Assim, não há o que ser removido
        if self.raiz == None:
            print('Lista Vazia\nNão há o que ser removido')
            return
        
        # Esse laço while irá varrer a estrutura verificando se a informação
        # que dado nodo tem é aquela que se está buscando. Caso não, o comando
        # nodoAtual = nodoAtual.proximoNodo passará a referência para o nodo
        # seguinte da estrutura, que terá sua informação verificada a seguir
        while nodoAtual.info != info:
            
            # Caso o nodo verificado seja o último, então não há a informação
            # a ser removida na lista
            if nodoAtual == ultimoNodo:
                print('Conteúdo não existe na estrutura')
                return
                
            nodoAtual = nodoAtual.proximoNodo
        
        # Se o nodo a ser removido for a raiz da lista, então modificamos a
        # raiz para o próximo nodo. Isso serve para evitar que a lista fique
        # sem raiz
        if nodoAtual == self.raiz:
            self.raiz = nodoAtual.proximoNodo
        
        # Se a função conseguiu sair do laço while então o conteúdo foi
        # encontrado e o nodo será removido. Para tanto, apenas atualiza-se
        # os ponteiros do nodo anterior ao atual para o próximoNodo do
        # nodoAtual, e do anterior ao próximo para o anterior ao atual,
        # removendo-o da estrutura
        nodoAtual.anteriorNodo.proximoNodo = nodoAtual.proximoNodo
        nodoAtual.proximoNodo.anteriorNodo = nodoAtual.anteriorNodo
        
        # Anulam-se os ponteiros próximo e anterior do nodo que está sendo
        # removido
        nodoAtual.anteriorNodo = None
        nodoAtual.proximoNodo = None
        
        return

    # Função para imprimir o conteúdo de um nodo da lista a partir
    # da posição informada (índice)
    #
    # posicao - índice a ter o conteúdo apresentado
    def acessaPosicao(self, posicao):
        # Guarda-se o nodo raiz da estrutura
        nodoAtual = self.raiz
        
        # Guarda-se o "último" nodo da lista circular, que será o nodo anterior
        # da raiz da lista
        ultimoNodo = self.raiz.anteriorNodo
        
        # Cria-se uma variável para ir guardando a posição em análise
        posicaoEmAnalise = 0
        
        # Percorre a estrutura até atingir a posição buscada ou até que o
        # nodoAtual seja o último nodo, o que significa que a posição é maior
        # que a quantidade de nodos existentes na estrutura
        while posicaoEmAnalise < posicao:
            if nodoAtual == ultimoNodo:
                break
            
            nodoAtual = nodoAtual.proximoNodo
            posicaoEmAnalise += 1
        
        # Caso posiao seja diferente da posicaoEmAnalise, então não há o que
        # mostrar; do contrário, imprime a informação do conteúdo fornecido
        if posicao == posicaoEmAnalise:
            print(nodoAtual.info)
        else:
            print('ERROR\nPosição Inválida!\nMaior que o final da lista')
        
        return

    # Função para buscar a posicao na lista de dado conteúdo
    #
    # conteudo - o conteudo a ser buscado na lista
    def buscaPosicao(self, conteudo):
        # Guarda-se o nodo raiz da estrutura
        nodoAtual = self.raiz
        
        # Guarda-se o "último" nodo da lista circular, que será o nodo anterior
        # da raiz da lista
        ultimoNodo = self.raiz.anteriorNodo
        
        # Cria-se uma variável para ir guardando a posição na estrutura
        posicao = 0
        
        # Percorre-se a estrutura em busca do conteúdo a ser encontrado.
        # Caso nodoAtual seja o último, então a busca percorreu a estrutura
        # inteira e não achou o conteúdo correspondente
        while nodoAtual.info != conteudo:
            if nodoAtual == ultimoNodo:
                break
            
            nodoAtual = nodoAtual.proximoNodo
            posicao += 1
        
        # Caso nodoAtual tenha o conteúdo buscado, imprime-se a informação; do
        # contrário, o conteúdo não existe na estrutura
        if nodoAtual.info == conteudo:
            print(posicao)
        else:
            print('Conteúdo não está presente')
        
        return

    # Função para imprimir o conteúdo de todos os nodos da lista
    def imprimeLista(self):
        nodoAtual = self.raiz
        ultimoNodo = self.raiz.anteriorNodo
        
        while nodoAtual != ultimoNodo:
            print(nodoAtual.info, end = ' ')
            nodoAtual = nodoAtual.proximoNodo
            
        print(nodoAtual.info, end = ' ')
        print()
        return
