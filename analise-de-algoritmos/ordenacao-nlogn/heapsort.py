def heapsort(l):
    for start in range(len(l) // 2, -1, -1):
        heapfy(l, start, len(l) - 1)
    for end in range(len(l) - 1, 0, -1):
        l[end], l[0] = l[0], l[end]
        heapfy(l, 0, end - 1)
    return l
