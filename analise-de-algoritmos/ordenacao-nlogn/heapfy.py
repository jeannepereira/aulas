def heapfy(l, root, end):
    child = root * 2
    
    while child < end:
        if child + 1 <= end and l[child] < l[child + 1]:
            child += 1
            
        if l[root] < l[child]:
            l[root], l[child] = l[child], l[root]
            root = child
            child = root * 2
        else:
            break
