\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage[brazilian]{babel}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{listings}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage[scaled=.95]{helvet}% helvetica as the origin of arial
\usepackage[helvet]{sfmath}    % for the mathematical enviroments
\renewcommand{\familydefault}{\sfdefault}

\newcommand{\specialcell}[2][c]{\begin{tabular}[#1]{@{}c@{}}#2\end{tabular}}

\lstset{showstringspaces=false}
%% ETH beamer theme
% Options: [default]
%   itemsblack/[itemsblue]: change color of bullets etc. to black/blue in
%   itemize style environments
%   [titlesblack]/titlesblue: change color of frame titles/subtitles to
%   black/blue
% \usetheme[itemsblack,titlesblack]{eth}
\usetheme{eth}

%% Theme uses ETH blue color by default. Can be changed to any color using this
%% command: 
\setbeamercolor{structure}{fg=ETHred}

%% Mandatory variables
\author{Filipe Saraiva}
\title{Algoritmos de Ordenação O(n log$_2$n)}
\subtitle{Análise de Algoritmos}
\institute{UFPA}
\date{\today}

\begin{document}

\begin{frame}
    \maketitle
\end{frame}

\begin{frame}{Conteúdo}
	\tableofcontents
\end{frame}

\section{Introdução}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Introdução}
Nessa apresentação serão discutidos os algoritmos de ordenação cuja complexidade
assintótica é O(n log$_2$n).
\end{frame}

\begin{frame}{Introdução}
Esses algoritmos se utilizam da estratégia de projeto de algoritmo
\textit{Dividir para Conquistar}, que consiste em dividir um problema maior em
um conjunto de problemas menores, resolvê-los, e em seguida unir suas resoluções
para resolver o problema maior.
\end{frame}

\begin{frame}{Introdução}
Essa estratégia reduz a quantidade de operações executadas pelo algoritmo, de
forma suficiente para reduzir sua complexidade computacional e torná-lo um
método mais interessante que os de complexidade O(n$^2$) vistos anteriormente.
\end{frame}

\begin{frame}{Introdução}
Dois algoritmos de complexidade O(n log$_2$n) serão estudados nesta aula:
\begin{itemize}
 \item Merge Sort;
 \item Heap Sort.
\end{itemize}
\end{frame}

\section{Merge Sort}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Merge Sort -- Introdução}
A estratégia de ordenação do Merge Sort é dividir a lista original nos seus
componentes fundamentais (os elementos em si), formando um conjunto de listas de
tamanho unitário.
\newline\newline
Em seguida, essas listas são ``misturadas'' (o \textit{merge} que dá nome ao
método) em ordem crescente, até que ao final a lista original terá sido ordenada
corretamente.
\end{frame}

\begin{frame}{Merge Sort -- Algoritmo (Pt. 1)}
\lstinputlisting[language=Python]{mergesort.py}
\end{frame}

\begin{frame}{Merge Sort -- Algoritmo (Pt. 2)}
\lstinputlisting[language=Python]{merge.py}
\end{frame}

\begin{frame}{Merge Sort -- Explicação}
Iniciando com a primeira função (``mergesort''), o algoritmo verifica na
primeira instrução se a lista tem tamanho menor ou igual a 1. Nesse caso, a
lista já está ``ordenada'' e o resultado pode ser retornado automaticamente.
\newline\newline
Caso contrário, o algoritmo calcula o \textbf{mid}, que vem a ser o índice que
aponta para o meio da lista (arredondando para baixo). Esse valor será utilizado
para separar a lista no \textbf{return}: perceba que ele retorna o resultado da
função ``merge'', que tem 2 argumentos: o primeiro é o mergesort da primeira
metade da lista, e o segundo é o mergesort da segunda metade da lista.
\end{frame}

\begin{frame}{Merge Sort -- Explicação}
Portanto, ``mergesort'' é um algoritmo recursivo, onde o caso base é atingido
quando a lista tem tamanho 1 ou menor que 1.
\newline\newline
Assim, essa etapa será chamada, recursivamente, até que a lista original seja
dividida em várias sublistas de tamanho 1 ou vazias. A cada chamada do
``mergesort'', a lista passada como argumento será dividida ao meio, até que a
lista chegue ao tamanho 1 ou vazia.
\end{frame}

\begin{frame}{Merge Sort -- Explicação}
Para ilustrar essa situação, imagine uma lista \textit{l} onde
$l = [5, 3, 1, 2]$. Representando as chamadas ao ``mergesort'' por ``MS'' e
as chamadas a ``merge'' por ``M'', teríamos a seguinte sequência de chamadas
para dividir a lista, por nível:
\newline\newline
MS(l)\\
MS([5, 3, 1, 2])\\
M(MS([5, 3]), MS([1, 2]))\\
M(M(MS([5]), MS([3])), M(MS([1]), MS([2])))\\
M(M([5], [3]), M([1], [2]))
\end{frame}

\begin{frame}{Merge Sort -- Explicação}
Dividida a lista, agora temos valores para que a função ``merge'' possa atuar.
Essa função fará a junção, em ordem, das listas correspondentes em seus dois
argumentos.
\newline\newline
As avaliações dos dois primeiros \textbf{if} verificam se alguma das listas é
vazia. Caso positivo, então a outra lista é retornada por completo pois ela já
está ordenada.
\newline\newline
Caso as duas listas tenham elementos, o bloco \textbf{if-else} referente ao
terceiro \textbf{if} é utilizado. Nele, comparam-se apenas os primeiros
elementos (índices 0) das listas \textit{l1} e \textit{l2}.
\end{frame}

\begin{frame}{Merge Sort -- Explicação}
Caso o primeiro elemento de \textit{l1} seja menor que o primeiro elemento de
\textit{l2}, então esse primeiro elemento será retirado de \textit{l1} e
concatenado à saída de ``merge'' aplicado às listas \textit{l1} sem o primeiro
elemento, e \textit{l2} completa. Toda essa saída está no \textbf{return}
relacionado ao terceiro \textbf{if}.
\newline\newline
Caso o elemento menor seja o primeiro elemento de \textit{l2}, então o retorno
será o primeiro elemento de \textit{l2} concatenado à saída de ``merge''
aplicado a lista \textit{l1} e a lista \textit{l2} sem o primeiro elemento. Essa
operação está descrita no \textbf{return} relacionado ao \textbf{else}.
\end{frame}

\begin{frame}{Merge Sort -- Explicação}
Assim, de maneira ordenada, o ``merge'' sempre retirará o menor elemento das
duas listas e enviará o que sobrou delas para ser ordenado recursivamente, o que
fará com que o próximo menor elemento seja retirado e assim sucessivamente até
termos a lista ordenada.
\newline\newline
Como o ``merge'' se inicia com as listas de tamanho unitário ou vazio, o método
tem garantias que, caso alguma das listas esteja vazia, a outra lista já estará
ordenada, retornando-a assim por inteira -- é o que acontece nos 2 primeiros
\textbf{if}.
\end{frame}

\begin{frame}{Merge Sort -- Explicação}
Voltando ao exemplo da ordenação da lista $l$, executaremos agora as operações
de ``merge'':
\newline\newline
M(M([5], [3]), M([1], [2]))\\
M([3] + M([5], []), [1] + M([], [2]))\\
M([3] + [5], [1] + [2])\\
M([3, 5], [1, 2])\\
$[1]$ + M([3, 5], [2])\\
$[1]$ + [2] + M([3, 5], [])\\
$[1]$ + [2] + [3, 5]\\
$[1, 2, 3, 5]$
\end{frame}

\begin{frame}{Merge Sort -- Explicação}
O Merge Sort da maneira como está implementado é composto por 2 funções, ambas
recursivas.
\newline\newline
A primeira, ``mergesort'', faz 2 chamadas recursivas onde, a cada chamada, a
entrada é dividida por 2, de maneira equilibrada.
\newline\newline
Assim, a função de recorrência dessa função pode ser descrita da seguinte forma,
onde $n$ é o tamanho da entrada:
\newline\newline
T($n$) = $\begin{cases}
  1\text{, se } n <= 1;\\
  2\text{T}(n/2) + 1\text{, se } n > 1
 \end{cases}$
\end{frame}

\begin{frame}{Merge Sort -- Explicação}
Utilizando Árvore de Recursão, essa função pode ser resolvida da seguinte forma:
T($n$) = $\begin{cases}
  1\text{, se } n <= 1;\\
  2\text{T}(n/2) + 1\text{, se } n > 1
 \end{cases}$
 \begin{center}
 \includegraphics[scale=0.5]{mergesort-arvore.png}
 \end{center}
\end{frame}

\begin{frame}{Merge Sort -- Explicação}
Ou seja, para a função ``mergesort'' a complexidade é O(log$_2$n). Como ela está
inserida na função ``merge'', teremos agora que calcular a complexidade de
``merge'' e multiplicar pela complexidade do ``mergesort''.
\newline\newline
A função ``merge'' também é recursiva. Façamos o tamanho da entrada \textit{n} 
ser a soma dos tamanhos de \textit{l1} e \textit{l2}.
\end{frame}

\begin{frame}{Merge Sort -- Explicação}
Na função ``merge'', percebemos que o caso base ocorre quando \textit{l1} ou
\textit{l2} são vazias. Até lá, o método vai fazendo chamadas recursivas, no
bloco \textbf{if-else}, sempre reduzindo o tamanho original \textit{n} em 1
unidade.
\newline\newline
Assim, a função de recorrência do ``merge'' pode ser expressa pela seguinte
fórmula:
\newline\newline
T($n$) = $\begin{cases}
  1\text{, se } l1 \text{ ou } l2 == [];\\
  \text{T}(n - 1) + 1\text{, se } l1 \text{ ou } l2 \neq []
 \end{cases}$
\end{frame}

\begin{frame}{Merge Sort -- Explicação}
Utilizando Árvore de Recursão, essa função pode ser resolvida da seguinte forma:
T($n$) = $\begin{cases}
  1\text{, se } l1 \text{ ou } l2 == [];\\
  \text{T}(n - 1) + 1\text{, se } l1 \text{ ou } l2 \neq []
 \end{cases}$
 \begin{center}
 \includegraphics[scale=0.5]{merge-arvore.png}
 \end{center}
\end{frame}

\begin{frame}{Merge Sort -- Explicação}
Assim, a função ``merge'' tem complexidade assintótica linear, O(n).
\newline\newline
Realizando o produto da complexidade das duas funções, teremos que a resultante
do algoritmo completo será O(n log$_2$n).
\newline\newline
Cabe observar também que, para ambos os algoritmos, mesmo no melhor caso tanto o
``mergesort'' quando o ``merge'' farão o mesmo número de operações do pior caso:
``mergesort'' terá que dividir a lista original em diversas sublistas de tamanho
unitário, e o ``merge'' terá que varrer ambas as listas até que alguma seja
vazia.
\newline\newline
Ou seja, no melhor caso o Merge Sort terá a mesma complexidade que no pior caso:
$\Omega$(n log$_2$n).
\end{frame}

\begin{frame}{Merge Sort -- Explicação}
O Merge Sort tem uma boa complexidade e é estável tanto no pior quanto no melhor
caso. A escolha do índice no meio da lista original divide o problema de forma
equilibrada, sendo portanto uma vantagem comparada com a maneira como o Quick
Sort realiza a divisão da lista.
\newline\newline
A principal desvantagem do algoritmo são os números de chamadas recursivas, que
demandam mais memória que métodos iterativos. Para a implementação apresentada
nessa apresentação, a função ``merge'' pode ser reimplementada para uma forma
iterativa, o que reduziria a quantidade de chamadas recursivas nesse exemplo.
\end{frame}

\section{Heap Sort}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\subsection{Introdução}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsection]
\end{frame}

\begin{frame}{Heap Sort -- Introdução}
O próximo algoritmo da classe O(n log$_2$n) que estudaremos será o Heap Sort.
Esse algoritmo se utiliza de uma estrutura de dados chamada Heap
(ou Árvore Heap), mais especificamente da Heap-Max.
\newline\newline
Portanto, antes de apresentarmos o algoritmo em si vamos estudar o que seriam as
estruturas Heap.
\end{frame}

\subsection{Estruturas Heap}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsection]
\end{frame}

\begin{frame}{Estruturas Heap}
Uma Heap é uma árvore binária balanceada derivada de uma lista (ou vetor) que
guarda algumas propriedades para seus índices. Fazendo \textit{i} representar
qualquer índice da estrutura (tanto do vetor quanto da árvore), tem-se:
\begin{itemize}
 \item $\lfloor$ i/2 $\rfloor$ é o pai do índice \textit{i};
 \item 2$i$ é o filho esquerdo do índice \textit{i};
 \item 2$i$ + 1 é o filho direito do índice \textit{i}.
\end{itemize}
Onde o índice 1 não tem pai; um índice \textit{i} só tem filho esquerdo se 
2$i$ $\leq$ $n$ (onde $n$ é o tamanho da lista); e $i$ só tem filho direito se 
2$i$ + 1 $\leq$ $n$.
\end{frame}

\begin{frame}{Estruturas Heap}
Para exemplificar, façamos a lista $l = [5, 3, 1, 2]$. Uma Heap derivada de $l$
seria:
 \begin{center}
 \includegraphics[scale=0.7]{heap.eps}
 \end{center}
Perceba que é possível navegar pelos índices (fazendo o primeiro índice ser 1)
de acordo com as propriedades de relações entre os índices listada
anteriormente.
\end{frame}

\begin{frame}{Estruturas Heap}
Existem 2 subtipos de estruturas Heap de grande interesse para a área: a Heap
Min e a Heap Max.
\newline\newline
A primeira diz respeito a aquelas onde todos os nós pai são menores que qualquer
de seus filhos.
\newline\newline
A lista $l$ reordenada para representar uma Heap Min teria a seguinte forma:
\begin{center}
 \includegraphics[scale=0.7]{heap-min.eps}
 \end{center}
\end{frame}

\begin{frame}{Estruturas Heap -- Heap Max}
Já a Heap Max são aqueles onde os nós pai são sempre maiores que qualquer de
seus filhos.
\newline\newline
A versão Heap Max da lista $l$ seria:
\begin{center}
 \includegraphics[scale=0.7]{heap.eps}
 \end{center}
Expressando em termos de funções matemáticas, uma Heap Max é uma Heap onde para
todo índice $i$, $l$[$i$] $\geq$ $l$[2$i$] e $l$[$i$] $\geq$ $l$[2$i$ + 1]
sempre que os índices não ultrapassarem $n$.
\end{frame}

\begin{frame}{Estruturas Heap -- Heap Max}
Para criar uma Heap Max é necessário reordenar a lista original de forma a fazer
cumprir os requisitos citados anteriormente.
\newline\newline
O algoritmo que produz uma Heap Max é chamado de ``heapfy'' e é apresentado no
slide seguinte:
\end{frame}

\begin{frame}{Estruturas Heap -- Heapfy}
\lstinputlisting[language=Python]{heapfy.py}
\end{frame}

\begin{frame}{Estruturas Heap -- Heapfy}
Dado uma lista ($l$), um nó a ser verificado ($root$) e o índice que identifica
o final da lista ($end$), o algoritmo ``heapfy'' servirá para criar a Heap Max
de uma dada lista.
\newline\newline
O primeiro comando, \textbf{child = root * 2}, servirá para identificarmos quem
é o ``filho da esquerda'' do nó $root$. Entrando no \textbf{while}, o primeiro
\textbf{if} verifica se o filho da direita (\textit{l[child + 1]}) está dentro
dos limites da lista e se ele não é maior que o filho da esquerda. Caso
afirmativo, faremos a comparação com esse filho. O importante é utilizarmos para
comparação o maior filho de $root$.
\end{frame}

\begin{frame}{Estruturas Heap -- Heapfy}
Essa comparação é realizada no segundo \textbf{if}. Se \textit{l[root] <
l[child]}, então temos que a árvore não é uma Heap Max e portanto temos que
trocar esses valores (o que acontece em \textbf{l[root], l[child] = l[child],
l[root]}).
\newline\newline
Em seguida, \textbf{root = child} servirá para que o algoritmo avalie o $child$
anterior como o novo $root$ da próxima iteração. Se aplicarmos esse algoritmo
sucessivamente, os maiores elementos ``subirão'' a árvore, enquanto os menores
``descerão'', até que a Heap Max esteja construída.
\end{frame}

\begin{frame}{Estruturas Heap -- Heapfy}
Avaliando que o algoritmo ``heapfy'' é executado enquanto \textbf{child < end},
e \textit{child} é duplicado a cada iteração que entra no segundo \textbf{if},
o algoritmo terá complexidade O(log$_2$n).
\end{frame}

\subsection{Heap Sort}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsection]
\end{frame}

\begin{frame}{Heap Sort -- Introdução}
Visto a estrutura Heap, bem como o subtipo Heap Max e o algoritmo ``heapfy'',
agora é chegada a hora de finalmente estudarmos como funciona o algoritmo de
ordenação Heap Sort.
\newline\newline
Grosso modo, o Heap Sort se utilizará da Heap Max para, sucessivamente,
encontrar os maiores elementos da lista e enviá-los para o final da mesma,
dividindo a lista original em uma parte ordenada (o final da mesma) e outra
parte a ser ordenada (o início da lista).
\newline\newline
O algoritmo Heap Sort é apresentado a seguir.
\end{frame}

\begin{frame}{Heap Sort -- Algoritmo}
\lstinputlisting[language=Python]{heapsort.py}
\end{frame}

\begin{frame}{Heap Sort -- Explicação}
Do algoritmo, percebe-se de imediato que ele utiliza 2 laços \textbf{for} não
aninhados. No primeiro, a instrução \textbf{range(len(l) // 2, -1, -1)}
significa que ele varrerá o intervalo de índices iniciando na metade do tamanho
da lista $l$ arredondada para baixo até o índice 0 (por isso aquele segundo
-1), e a cada iteração será decrementado em -1 (o último argumento da
instrução).
\newline\newline
Para cada iteração a função ``heapfy'' será invocada. Esse laço \textbf{for},
portanto, criará a árvore Heap Max inicial do método.
\end{frame}

\begin{frame}{Heap Sort -- Explicação}
Já o segundo \textbf{for} varre os índices do tamanho da lista -1 até o índice 1
(\textbf{range(len(l) - 1, 0, -1)}).
\newline\newline
A instrução seguinte é bastante interessante para entendermos o que o Heap Sort
está fazendo. Após criar a Heap Max no \textbf{for} anterior, sabemos que o
maior elemento da árvore está no primeiro índice da lista ($l[0]$). Assim, o
algoritmo o envia para o final da lista (\textbf{l[end], l[0] = l[0], l[end]}),
e coloca o elemento que estava no final no início.
\newline\newline
Assim, o Heap Sort irá sempre encontrar os maiores elementos e enviá-los para a
parte final da lista.
\end{frame}

\begin{frame}{Heap Sort -- Explicação}
Perceba que com a alteração da lista, a Heap Max não está desfeita. Para
corrigir isso o método chama ``heapfy'', que irá rearranjar a lista para gerar
a Heap Max.
\newline\newline
Veja o último argumento da chamada a ``heapfy'' nesse segundo \textbf{for}: o
\textit{end} está sendo decrementado a cada iteração. Isso significa que o
algoritmo aponta para o final da lista desordenada. Como a cada iteração o maior
elemento da parte desordenada da lista é enviado para o final da mesma, esse
$end$ deve ser decrementado para indicar a parte final da lista desordenada.
\end{frame}

\begin{frame}{Heap Sort -- Explicação}
Ao finalizar todas as iterações desse segundo \textbf{for}, a lista estará
ordenada.
\newline\newline
O segundo \textbf{for} executa $n$ - 1 vezes e é a instrução de maior custo
dessa função. Portanto, a complexidade dela será O(n).
\newline\newline
Como o ``heapfy'' é invocado no segundo \textbf{for}, ele será executado também
$n$ vezes. Como o ``heapfy'' já tem complexidade O(log$_2$n), a complexidade
total do algoritmo será o produto entre essas duas complexidades destacadas:
O(n log$_2$n).
\newline\newline
A exemplo do Merge Sort, cabe destacar que ambas as funções do Heap Sort terão
todas as suas operações executadas mesmo no melhor caso, o que confere a
complexidade desse algoritmo ser também $\Omega$(n log$_2$n).
\end{frame}

\begin{frame}{Heap Sort -- Exemplo}
Exemplo: $l = [5, 3, 1, 2]$ -- as figuras do lado esquerdo mostram a Heap Max
enquanto no direito a Heap resultado da alteração que precisa ser corrigida.
 \begin{center}
 \includegraphics[scale=0.7]{heap-exemplo-1.eps}
 \end{center}
\end{frame}

\begin{frame}{Heap Sort -- Exemplo (Continuação)}
Exemplo: $l = [5, 3, 1, 2]$ -- as figuras do lado esquerdo mostram a Heap Max
enquanto no direito a Heap resultado da alteração que precisa ser corrigida.
 \begin{center}
 \includegraphics[scale=0.7]{heap-exemplo-2.eps}
 \end{center}
\end{frame}

\begin{frame}{Heap Sort -- Exemplo (Continuação)}
Exemplo: $l = [5, 3, 1, 2]$ -- as figuras do lado esquerdo mostram a Heap Max
enquanto no direito a Heap resultado da alteração que precisa ser corrigida.
 \begin{center}
 \includegraphics[scale=0.7]{heap-exemplo-3.eps}
 \end{center}
\end{frame}

\section{Conclusões}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Conclusões}
 \begin{itemize}
  \item Nessa apresentação estudamos alguns algoritmos de complexidade
  O(n log$_2$n): Merge Sort e o Heap Sort;
  \item Ambos os algoritmos tem complexidade tanto no pior caso quanto no
  melhor idênticas;
  \item O Merge Sort utiliza muita recursividade, o que pode ser um ponto
  negativo em termos de desempenho;
  \item Já o Heap Sort utiliza estruturas Heap Max para encontrar elementos
  maiores e enviá-lo para o final da parte da lista a ser ordenada.
 \end{itemize}
\end{frame}

\begin{frame}
    \maketitle
\end{frame}

\end{document}
