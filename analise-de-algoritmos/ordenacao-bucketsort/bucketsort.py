def bucketsort(l):
    buckets = [[]] * (max(l) + 1)
    for i in l:
        buckets[i] = buckets[i] + [i]
    x = []
    for i in buckets:
        x = x + i
    return x
