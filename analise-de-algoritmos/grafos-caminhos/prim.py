g = [[0,  0, 17, 5, 15, 0],
     [0,  0, 0,  0, 7,  3],
     [17, 0, 0,  0, 2,  0],
     [5,  0, 0,  0, 0,  4],
     [15, 7, 2,  0, 0,  21],
     [0,  3, 0,  4, 21,  0]]

def prim(g):

    import numpy
    inf = numpy.inf

    custo = [inf for i in range(6)]
    anterior = [0 for i in range(6)]
    avaliado = [False for i in range(6)]

    noFonte = 0
    custo[noFonte] = 0
    while avaliado.__contains__(False):

        minIndiceCusto = 0
        for i in range(len(custo)):
            if custo[i] < custo[minIndiceCusto] and not avaliado[i]:
                minIndiceCusto = i

        for i in range(len(g)):
            if avaliado[i] == False and g[minIndiceCusto][i] > 0 and custo[i] > g[minIndiceCusto][i]:
                custo[i] = g[minIndiceCusto][i]
                anterior[i] = minIndiceCusto

        avaliado[minIndiceCusto] = True
        custo[noFonte] = inf

    solucao = []
    for i in range(1, len(anterior)):
        solucao.append([i, anterior[i]])

    return solucao
