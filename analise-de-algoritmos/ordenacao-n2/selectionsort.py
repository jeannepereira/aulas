def selectionsort(l):
    for i in range(0, len(l) - 1):
        x = i
        for j in range(i + 1, len(l)):
            if l[j] < l[x]:
                x = j
        if x != i:
            l[x], l[i] = l[i], l[x]
    return l
