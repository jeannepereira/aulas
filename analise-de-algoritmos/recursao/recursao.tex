\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage[brazilian]{babel}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{listings}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage[scaled=.95]{helvet}% helvetica as the origin of arial
\usepackage[helvet]{sfmath}    % for the mathematical enviroments
\renewcommand{\familydefault}{\sfdefault}

\newcommand{\specialcell}[2][c]{\begin{tabular}[#1]{@{}c@{}}#2\end{tabular}}

\lstset{showstringspaces=false}
%% ETH beamer theme
% Options: [default]
%   itemsblack/[itemsblue]: change color of bullets etc. to black/blue in
%   itemize style environments
%   [titlesblack]/titlesblue: change color of frame titles/subtitles to
%   black/blue
% \usetheme[itemsblack,titlesblack]{eth}
\usetheme{eth}

%% Theme uses ETH blue color by default. Can be changed to any color using this
%% command: 
\setbeamercolor{structure}{fg=ETHred}

%% Mandatory variables
\author{Filipe Saraiva}
\title{Projeto de Algoritmos -- Recursão}
\subtitle{Análise de Algoritmos}
\institute{UFPA}
\date{\today}

\begin{document}

\begin{frame}
    \maketitle
\end{frame}

\begin{frame}{Conteúdo}
	\tableofcontents
\end{frame}

\section{Introdução -- Recursão}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Introdução -- Recursão}
Nesta aula veremos o projeto de algoritmo chamado \textbf{Recursão} ou
\textbf{Recursividade}. Este projeto não é inteiramente novo para nós: quando 
resolvíamos recorrências, estávamos buscando a complexidade de algoritmos 
recursivos.
\newline\newline
Veremos em maiores detalhes as características, requisitos de implementação, e 
exemplos dessa técnica.
\end{frame}

\section{Características da Recursão}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Características da Recursão}
Um procedimento que chama a si mesmo é dito \textbf{recursivo}.
\newline\newline
Exemplo: Cálculo do Fatorial em forma recursiva
\newline
\lstinputlisting[language=Python]{../algoritmos-implementacao/fatorial_rec.py}
\end{frame}

\begin{frame}{Características da Recursão}
Algoritmos recursivos realizam o processamento das chamadas recursivas através 
de uma pilha, que vai armazenando as variáveis a cada iteração recursiva.
\newline\newline
Quando o caso de terminação é atingido, a saída vai retornando para os 
processamentos pendentes, finalizando cada um até obtenção da resposta.
\end{frame}

\begin{frame}{Características da Recursão}
A utilização da pilha atribui um limite máximo de utilização de memória para a 
recurção.
\newline\newline
Cada chamada recursiva faz alocações de memórias, significando que caso o método 
realize muitas recurções pode resultar em um estouro de memória no sistema.
\end{frame}

\begin{frame}{Características da Recursão}
Também é importante se preocupar com o ``problema da parada'' do algoritmo 
recursivo.
\newline\newline
Todo algoritmo recursivo deve ter um critério onde não haverá mais chamadas 
recursivas, atingindo-se o término do método.
\end{frame}

\begin{frame}{Características da Recursão}
Esse critério deve ser bem projetado e deve haver garantias de que ele será 
atingido em algum momento.
\newline\newline
Normalmente, em algoritmos recursivos a entrada sofre algum processamento, como 
redução, divisão, ou outros, garantindo que ela será alterada para a próxima 
chamada recursiva, até que ela atinga o caso de terminação.
\end{frame}

\begin{frame}{Características da Recursão}
No geral, algoritmos recursivos são concisos e fáceis de entender. Também são 
muito elegantes em termos de implementação.
\newline\newline
Há mesmo diferentes paradigmas e linguagens de programação que são fortemente 
baseados em recursão, como Programação Lógica (Prolog) e Programação Funcional 
(Haskell, LISP).
\end{frame}

\section{Exemplos}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}[fragile]{Exemplos}
Exemplo 1: Fatorial -- Complexidade: O(n)
\newline
\lstinputlisting[language=Python]{../algoritmos-implementacao/fatorial_rec.py}
\end{frame}

\begin{frame}[fragile]{Exemplos}
Exemplo 2: Busca Binária -- Complexidade: O(log n)
\newline
\lstinputlisting[language=Python]{
../algoritmos-implementacao/buscabinaria_rec.py}
\end{frame}

\begin{frame}[fragile]{Exemplos}
Exemplo 3: Algoritmo Euclidiano
\newline
\lstinputlisting[language=Python]{../algoritmos-implementacao/euclidian.py}
\end{frame}

\begin{frame}[fragile]{Exemplos}
Exemplo 4: Busca Linear (Prolog)
\newline
\lstinputlisting[language=Prolog]{../algoritmos-implementacao/busca.pro}
\end{frame}

\begin{frame}[fragile]{Exemplos}
Exemplo 5: Autômato Finito Determinístico (Prolog)
\newline
\begin{center}
\includegraphics[scale=0.65]{dfa.png}
\end{center}
\newline
Disponível em \url{https://gitlab.com/filipesaraiva/dfa}
\end{frame}

\begin{frame}[fragile]{Exemplos}
Exemplo 6: Número de Fibonacci -- Complexidade O(2$^n$)
\newline
\lstinputlisting[language=Python]{../algoritmos-implementacao/fibonacci_rec.py}
\end{frame}

\section{Quando não usar Recursão}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Quando não usar Recursão}
Muitas vezes, implementações recursivas podem levar a complexidades intratáveis 
aos procedimentos projetados.
\newline\newline
Nesses casos, cabe avaliar se uma implementação iterativa convencional do método 
é mais eficiente que a recursiva.
\end{frame}

\begin{frame}{Quando não usar Recursão}
Exemplo 7: Número de Fibonacci Iterativo -- Complexidade O(n)
\newline
\lstinputlisting[language=Python]{../algoritmos-implementacao/fibonacci_it.py}
\end{frame}

\begin{frame}{Quando não usar Recursão}
Exemplo 7: Número de Fibonacci Iterativo -- Complexidade O(n)
\newline
\lstinputlisting[language=Python]{../algoritmos-implementacao/fibonacci_it.py}
\begin{center}
\begin{tabular}{|c|c|c|c|c|}
\hline
\textbf{n} & \textbf{20} & \textbf{30} & \textbf{50} & \textbf{100}\\\hline
\textbf{Recursivo} & 1 s & 2 min & 21 dias & 10$^9$ anos\\ \hline
\textbf{Iterativo} & 1/3 ms & 1/2 ms & 3/4 ms & 3/2 ms\\ \hline
\end{tabular}
\end{center}

\end{frame}

\section{Conclusões}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Conclusões}
 \begin{itemize}
  \item Recursão ou Recursividade é o nome dado a um método que chama a si
  próprio para realizar algum trabalho;
  \item Todo método recursivo necessita de um caso de término para evitar loops
  infinitos. Esse caso de término deve ser atingido;
  \item Deve-se tomar cuidado ao verificar se um método recursivo não irá
  aumentar significativamente a complexidade do método de solução quando
  comparado com um método iterativo convencional.
 \end{itemize}
\end{frame}

\begin{frame}
    \maketitle
\end{frame}

\end{document}
