class ListaSequencial:
    
    def __init__(self, tamanhoMaximo):
        self.lista = []
        self.tamanhoMaximo = tamanhoMaximo
        self.proximoIndice = 0

    # Com uso do ponteiro proximoIndice
    def insereNoFim(self, x):
        
        if self.proximoIndice > self.tamanhoMaximo:
            print('ERROR\nLista com tamanho máximo permitido')
            return
        
        self.lista.append(None)
        self.lista[self.proximoIndice] = x
        self.proximoIndice += 1
        
        return

    # Sem ponteiro proximoIndice para navegar
    def insereNoFim(self, x):
        
        tamanhoLista = 0
        for i in self.lista:
            tamanhoLista += 1

        if tamanhoLista >= self.tamanhoMaximo:
            print('ERROR\nLista com tamanho máximo permitido')
            return

        self.lista.append(None)
        self.lista[tamanhoLista] = x
        
        return
    
