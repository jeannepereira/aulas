def fatorial_rec(n):
    if n == 1:
        return 1
    else:
        return n * fatorial_rec(n - 1)
