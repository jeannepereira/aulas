def buscabinaria_it(l, x):
    menor = 0
    maior = len(l) - 1
    
    while menor <= maior:
        mid = (menor + maior) // 2
        
        if x == l[mid]:
            return True
        elif x < l[mid]:
            maior = mid - 1
        else:
            menor = mid + 1
        
    return False
