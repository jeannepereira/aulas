class PilhaSequencial:
    
    def __init__(self, tamanhoMaximo):
        self.pilha = []
        self.tamanhoMaximo = tamanhoMaximo
        self.topo = 0

    # Com uso do ponteiro proximoIndice
    def insere(self, x):
        
        if self.topo >= self.tamanhoMaximo:
            print('ERROR\nPilha com tamanho máximo permitido')
            return
        
        self.pilha.append(None)
        self.pilha[self.topo] = x
        self.topo += 1
        
        return

    # Com uso do ponteiro proximoIndice
    def remove(self):
        
        self.pilha[self.topo] = None
        self.topo -= 1
        
        return
    
