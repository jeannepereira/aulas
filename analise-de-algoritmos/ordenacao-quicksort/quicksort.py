def quicksort(l):
    if len(l) <= 1:
        return l
    pivot = l[len(l) // 2]
    menor, igual, maior = [], [], []
    for i in l:
        if i < pivot:
            menor = menor + [i]
        elif i == pivot:
            igual = igual + [i]
        else:
            maior = maior + [i]
    return quicksort(menor) + igual + quicksort(maior)
