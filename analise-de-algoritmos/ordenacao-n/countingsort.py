def countingsort(l):
    aux = [0] * (max(l) + 1)
    for i in l:
        aux[i] += 1
    soma = 0
    for i in range(0, len(aux)):
        dum = aux[i]
        aux[i] = soma
        soma += dum
    output = [0] * (len(l))
    for i in l:
        output[aux[i]] = i
        aux[i] += 1
    return output
